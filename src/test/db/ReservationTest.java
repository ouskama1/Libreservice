package db;

import base.TestBase;
import com.fort.kickass.base.Utils;
import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.service.impl.LibraryService;
import com.fort.kickass.service.impl.ReservationService;
import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.dto.UsersDto;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by kommar on 30/05/2017.
 */
public class ReservationTest extends TestBase{

    private ReservationDto createdDto;
    private ReservationService reservationService;
    private BookService bookService;
    private UsersService usersService;

    @Before
    public void initTest() {
        reservationService = injector.getInstance(ReservationService.class);
        bookService = injector.getInstance(BookService.class);
        usersService = injector.getInstance(UsersService.class);
    }

    @Test
    public void reservationTest() throws ParseException {
        Random rand = new Random();
        int randomNumba = rand.nextInt(2017) + 2000;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "31-08-"+randomNumba;
        Date date = sdf.parse(dateInString);

        UsersDto person = (UsersDto)usersService.getSearchedUsers("").get(0);
        BookDetailDto book = (BookDetailDto)bookService.getBook(1);

        createdDto = new ReservationDto();
        createdDto.setDateFrom(Utils.dateToCalendar(date));
        createdDto.setExpectedDateTo(Utils.dateToCalendar(date));
        createdDto.setCheckedOut(false);
        createdDto.setPerson(person);
        createdDto.setBook(book);

        reservationService.createReservation(createdDto);

        ReservationDto dtoFromDB = (ReservationDto)reservationService.getReservationsFromTo(Utils.dateToCalendar(date), Utils.dateToCalendar(date), book.getId()).get(0);

        assertEquals(createdDto.getDateFrom(), dtoFromDB.getDateFrom());
        assertEquals(createdDto.getExpectedDateTo(), dtoFromDB.getExpectedDateTo());
        assertEquals(createdDto.isCheckedOut(), dtoFromDB.isCheckedOut());
        assertEquals(createdDto.getPerson().getId(), dtoFromDB.getPerson().getId());
        assertEquals(createdDto.getBook().getId(), dtoFromDB.getBook().getId());

        reservationService.checkOutReservation(dtoFromDB.getId());

        dtoFromDB = (ReservationDto)reservationService.getReservationsFromTo(Utils.dateToCalendar(date), Utils.dateToCalendar(date), book.getId()).get(0);

        assertEquals(true, dtoFromDB.isCheckedOut());

        reservationService.deleteReservation(dtoFromDB.getId());

        List reservations = reservationService.getReservationsFromTo(Utils.dateToCalendar(date), Utils.dateToCalendar(date), book.getId());

        assertEquals(true, reservations.isEmpty());
    }
}
