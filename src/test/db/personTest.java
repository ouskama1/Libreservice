package db;

import base.TestBase;
import com.fort.kickass.base.Utils;
import com.fort.kickass.persistance.cdi.interceptor.TransactionalModule;
import com.fort.kickass.service.impl.LibraryService;
import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.UsersDto;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by kommar on 30/05/2017.
 */
public class personTest extends TestBase{

    private UsersDto createdDto;
    private AddressDto createdAddrDto;
    private UsersService usersService;
    private LibraryService libraryService;

    @Before
    public void initTest() {
        usersService = injector.getInstance(UsersService.class);
    }

    @Test
    public void createPerson() {
        Random rand = new Random();
        int randomNumba = rand.nextInt(999999) + 1;

        createdDto = new UsersDto();
        createdDto.setFirstName("Susannah");
        createdDto.setLastName("Dean");
        createdDto.setEmail("sudean@gmail.com" + randomNumba);
        createdDto.setPhone("721452365" + randomNumba);
        createdDto.setPassword(Utils.hashPassword("banana"));
        createdDto.setCardNumber(Utils.generateCardNumber());

        createdAddrDto = new AddressDto();
        createdAddrDto.setStreet("Pine street");
        createdAddrDto.setHouseNumber(3);
        createdAddrDto.setCity("Berlin");
        createdAddrDto.setCountry("Germany");
        createdAddrDto.setZipcode("851 02");
        createdAddrDto.setOtherDetails("");
        createdDto.setAddress(createdAddrDto);

        usersService.createUser(createdDto);

        UsersDto dtoFromDB = (UsersDto)usersService.getSearchedUsers(createdDto.getCardNumber()).get(0);

        assertEquals(createdDto.getCardNumber(), dtoFromDB.getCardNumber());
        assertEquals(createdDto.getFirstName(), dtoFromDB.getFirstName());
        assertEquals(createdDto.getLastName(), dtoFromDB.getLastName());
        assertEquals(createdDto.getEmail(), dtoFromDB.getEmail());
        assertEquals(createdDto.getPhone(), dtoFromDB.getPhone());
        assertEquals(createdDto.getPassword(), dtoFromDB.getPassword());
        assertEquals(createdDto.getAddress().getStreet(), dtoFromDB.getAddress().getStreet());
        assertEquals(createdDto.getAddress().getHouseNumber(), dtoFromDB.getAddress().getHouseNumber());
        assertEquals(createdDto.getAddress().getCity(), dtoFromDB.getAddress().getCity());
        assertEquals(createdDto.getAddress().getCountry(), dtoFromDB.getAddress().getCountry());
        assertEquals(createdDto.getAddress().getZipcode(), dtoFromDB.getAddress().getZipcode());
        assertEquals(createdDto.getAddress().getOtherDetails(), dtoFromDB.getAddress().getOtherDetails());
    }
}
