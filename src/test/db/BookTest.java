package db;

import base.TestBase;
import com.fort.kickass.base.Utils;
import com.fort.kickass.persistance.model.Library;
import com.fort.kickass.service.converter.impl.LibraryConverter;
import com.fort.kickass.service.impl.*;
import com.fort.kickass.view.dto.*;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by kommar on 30/05/2017.
 */
public class BookTest extends TestBase{

    private BookDetailDto createdDto;
    private ReservationService reservationService;
    private BookService bookService;
    private AuthorService authorService;
    private GenreService genreService;
    private LibraryService libraryService;

    @Before
    public void initTest() {
        reservationService = injector.getInstance(ReservationService.class);
        authorService = injector.getInstance(AuthorService.class);
        genreService = injector.getInstance(GenreService.class);
        bookService = injector.getInstance(BookService.class);
        libraryService = injector.getInstance(LibraryService.class);
    }

    @Test
    public void reservationTest() throws ParseException {
        createdDto = new BookDetailDto();
        List<AuthorDto> authorDtoList = new ArrayList();
        AuthorDto author = (AuthorDto)authorService.getAuthors().get(0);
        authorDtoList.add(author);
        List<GenreDto> genreDtoList = new ArrayList();
        GenreDto genre = (GenreDto)genreService.getGenres().get(0);
        genreDtoList.add(genre);
        Library library = (Library)new LibraryConverter().toData(libraryService.getLibrary(1));

        String isbn = Utils.generateCardNumber();
        createdDto.setIsbn(isbn);
        createdDto.setLibrary(library);
        createdDto.setAuthors(authorDtoList);
        createdDto.setGenres(genreDtoList);
        createdDto.setLanguage("EN");
        createdDto.setPages(1500);
        createdDto.setPublishedYear(2017);
        createdDto.setPublisher("Pinguins");
        createdDto.setTitle("THE BOOK");

        bookService.createBook(createdDto);

        BookDetailDto dtoFromDB = bookService.getBookWithIsbn(isbn);

        assertEquals(createdDto.getIsbn(), dtoFromDB.getIsbn());
        assertEquals(createdDto.getAuthors().get(0).getId(), dtoFromDB.getAuthors().get(0).getId());
        assertEquals(createdDto.getLanguage(), dtoFromDB.getLanguage());
        assertEquals(createdDto.getPublisher(), dtoFromDB.getPublisher());
        assertEquals(createdDto.getTitle(), dtoFromDB.getTitle());
        assertEquals(createdDto.getGenres().get(0).getId(), dtoFromDB.getGenres().get(0).getId());

        String expectedLanguage = "MOTHER RUSSIA";
        dtoFromDB.setLanguage(expectedLanguage);
        dtoFromDB.setAuthors(new ArrayList<AuthorDto>());
        bookService.editBook(dtoFromDB);

        dtoFromDB = bookService.getBookWithIsbn(isbn);

        assertEquals(expectedLanguage, dtoFromDB.getLanguage());
        assertEquals(false, dtoFromDB.getGenres().isEmpty());
        assertEquals(true, dtoFromDB.getAuthors().isEmpty());
    }
}
