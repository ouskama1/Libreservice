package base;

import com.fort.kickass.persistance.cdi.interceptor.TransactionalModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Created by Marcel on 01.06.2017.
 */
public class TestBase {
    protected Injector injector;

    public TestBase (){
        TransactionalModule module = new TransactionalModule();
        injector = Guice.createInjector(module);
    }
}
