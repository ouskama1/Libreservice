# *LibreService <br> <center>Architecture*</center>

![Architecture](/man/pictures/architecture.png)

## Persistence
The layer taking care of direct connection to the database (Hibernate).

### Criteria
Selects from the database according to chosen criteria.

### DAO
CRUD, as in create, read, update, and delete contains the four
basic functions of persistent storage.

### Model
The model contains the database model, its tables and details (entities).

---

## Service
The layer that communicates between persistence and the view.

### Converters
Convert the data to dtos and vice versa.

### Services
Classes communicating between persistence and the view.

---

## View
GUI layer - the user interface of the application (JavaFx).

### DTOs
Database entities transformed into objects that are used by the application view.

### Panels
Panels that can be switched like pages.