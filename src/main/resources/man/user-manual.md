# *LibreService <br> <center>User Manual*</center>

## Login page

![UserManualImage](/man/pictures/login.jpg)<br>
The login page contains two boxes: **Mail** and **Password**.
The user needs to enter both correctly in order to log in and use the page.
By clicking on the Login button, the user is taken to the application - to the Catalog.

## Main page and Top menu buttons

![UserManualImage](/man/pictures/topMenuBtns.jpg)<br>
The main page contains the top Menu, which holds:

1. Home button
2. Catalog button
3. Users button
4. Logout button

### Home

The homepage shows the up-to-date documentation for LibreService, including this manual.<br>
![UserManualImage](/man/pictures/homePageBtns.jpg)<br>
Two buttons are available:
1. **User manual**
Explains in detail how to use the application.
2. **Architecture**
Explains in detail how the application works internally and what it consists of.

### Catalog

The catalog consists of two tables:
1. The first table contains the book TITLES and their AUTHORS. Clicking on a book opens its details in the right-side table.<br>
![UserManualImage](/man/pictures/bookPaneLeft.jpg)<br>
2. The second table displays details for the selected book in the first table:<br>
![UserManualImage](/man/pictures/bookPaneCenter.jpg)<br>
**PUBLISHER**, **PUBLISHED YEAR**, **LANGUAGE** and whether or not the book can be found in the current **LIBRARY**.

The bottom right corner of the catalog pane shows the **Detail** button. Clicking on it is possible only after selecting a book.
The Detail button opens a page that shows all the details of the selected book and allows making a reservation.<br>
![UserManualImage](/man/pictures/bookDetailBtn.jpg)

#### Detail button - Reservation page

After selecting a book and clicking on the **Detail** button, the user is taken to the Reservations page.<br>
![UserManualImage](/man/pictures/reservationsTables.jpg)<br>

The page contains two columns:
1. **Book details** with a **Calendar** --- user can see information about the selected book. 
User can also see when the book is available and can select dates in *orange* and dates in *purple*.
2. **Reservations** --- by selecting the dates in *orange* or *purple*, the user can see who reserved the book or make
a reservation on the selected dates respectively.

### Users

The panel users contains a searchable list of users. It consists of three parts:
1. Left table: list of users according to their details
2. Right table: details of the users including the reservations of their name
3. **New user** button

#### List of users

By using the search bar, needed users can be selected.<br>
![UserManualImage](/man/pictures/searchUser.jpg)<br>
The search bar works with all the details that are stored for the users.
The table only shows *First name*, *Last name* and *Card number*.<br>
Clicking on a user opens his/her details on the right side of the page.

#### User details and reservations

All of user's details are shown on the right side of the page. User's reservations are also shown below.<br>
![UserManualImage](/man/pictures/userDetails.jpg)<br>
Under the reservations table, there are two buttons *Check out* and *Delete*

#### Checking out and deleting reservations
![UserManualImage](/man/pictures/checkDelete.jpg)<br>
1. **Check out** - the selected reservation will be marked as checked out.
2. **Delete** - the selected reservation will be deleted/cancelled.

#### New user

Underneath the left side table, there is a button named **New user**.<br>
![UserManualImage](/man/pictures/newUserBtn.jpg)<br>

By clicking on that button, a new form opens on the right side. The form consists of
two parts:
1. Personal information / Address<br>
![UserManualImage](/man/pictures/newUserInfo.jpg)<br>
The user information that needs to be filled out is: *Username, First name, Last name, Email, Phone, Password (repeated).*<br>
For the Address: *Street, House number, City, Zipcode, Country, Other details (optional).*
2. Employee information<br>
![UserManualImage](/man/pictures/employee.jpg)<br>
If the new user is an employee, the box in the top-right corner needs to be checked. That allows for entering the *Position* and the *Dates to* and *From* when the user is employed.

### Logout

By clicking on the *Logout* button, the user's session ends and the login page opens again.<br>
![UserManualImage](/man/pictures/logout.jpg)