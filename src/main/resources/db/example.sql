INSERT INTO address VALUES (nextval('address_id_seq'), 'Kroftova', 1, 'Praha 5', '150 00', 'Czech Republic', 'Tall building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Písecká', 846, 'Bechyně', '391 45', 'Czech Republic', 'Small building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Dlouhá', 154, 'Tábor', '391 65', 'Czech Republic', 'Medium building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Obránců míru', 54, 'Sezimovo Ústí', '546 78', 'Czech Republic', 'Largest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Bechyňská', 12, 'Tábor', '391 65', 'Czech Republic', 'Smallest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Krátká', 12, 'Praha 5', '150 00', 'Czech Republic', 'Tall building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Písecká', 1547, 'Praha', '391 45', 'Czech Republic', 'Small building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Dlouhá', 654, 'Praha', '391 65', 'Czech Republic', 'Medium building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Obránců', 88, 'Praha', '546 78', 'Czech Republic', 'Largest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Mír', 2, 'Praha', '391 65', 'Czech Republic', 'Smallest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Lanová', 1, 'Praha', '150 00', 'Czech Republic', 'Tall building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Modrá', 36, 'Praha', '391 45', 'Czech Republic', 'Small building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Zelená', 789, 'Střezimíř', '391 65', 'Czech Republic', 'Medium building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Bílá', 96, 'Střezimíř', '546 78', 'Czech Republic', 'Largest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Obrovská', 154, 'Střezimíř', '391 65', 'Czech Republic', 'Smallest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Krátkého', 362, 'Střezimíř', '150 00', 'Czech Republic', 'Tall building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Dlouhého', 5252, 'Plzeň', '391 45', 'Czech Republic', 'Small building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Dlouhá', 8585, 'Plzeň', '391 65', 'Czech Republic', 'Medium building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Obránců míru', 14, 'Plzeň', '546 78', 'Czech Republic', 'Largest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Bechyňská', 12, 'Plzeň', '391 65', 'Czech Republic', 'Smallest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Kroftova', 963, 'Plzeň', '150 00', 'Czech Republic', 'Tall building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Písecká', 951, 'Most', '391 45', 'Czech Republic', 'Small building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Dlouhá', 753, 'Most', '391 65', 'Czech Republic', 'Medium building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Obránců míru', 3535, 'Most', '546 78', 'Czech Republic', 'Largest building on the corner');
INSERT INTO address VALUES (nextval('address_id_seq'), 'Bechyňská', 7595, 'Most', '391 65', 'Czech Republic', 'Smallest building on the corner');



INSERT INTO person VALUES (nextval('person_id_seq'), 'Jane', 'Calamity', '$2a$10$.UpeA8GIiDjd3U9j8Xxk8u.ZSeJgabSiiuOc9dtmbh3wtXEwpU/9.' , 'calamity.jane@gmail.com', '773224466', 3, 'CJ125426');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jack', 'White', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.white@gmail.com', '602125485', 2, 'WJ1567816');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Alice', 'Mayor','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'malice@hotmail.com', '603125785', 4, 'MA215862');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Kate', 'Waiter', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w8r@gmail.com', null, 3, 'WK15682158');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Joe', 'Mansion','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'largehouse@yahoo.com', '775214523', 5, 'MJ1586132');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jane', 'Aghbad', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' , 'calamity1.jane@gmail.com', '773224401', 1, 'CJ1254261');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jack', 'Black', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.white2@gmail.com', '602125402', 2, 'WJ15678162');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Mark', 'Red','$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' , 'malice@hot4mail.com', '603125703', 3, 'MA2158623');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Kate', 'Blue', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'k8w8r@gm5ail.com', null, 4, 'WK156821584');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Marina', 'House','$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' , 'largehou6se@yahoo.com', '775214504', 6, 'MJ15861325');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Martina', 'Nováková', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' , 'calam78ity.jane@gmail.com', '773224400', 7, 'CJ1254266');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Alena', 'Nováková', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.whi98te@gmail.com', '602125405', 8, 'WJ15678167');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jaromír', 'Novák','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'mal78ice@hotmail.com', '603125706', 9, 'MA2158628');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Tomáš', 'Kovářík', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w898r@gmail.com', null, 10, 'WK156821589');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jack', 'Little','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'largeh56ouse@yahoo.com', '775214507', 11, 'MJ158613210');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Juno', 'Big', '$2a$10$.UpeA8GIiDjd3U9j8Xxk8u.ZSeJgabSiiuOc9dtmbh3wtXEwpU/9.' , 'calami45ty.jane@gmail.com', '773224408', 12, 'CJ12542611');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Huno', 'Large', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.whi56te@gmail.com', '602125409', 13, 'WJ156781612');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Hodor', 'Huge','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'malice@hotma5646il.com', '603125710', 14, 'MA21586213');
INSERT INTO person VALUES (nextval('person_id_seq'), 'John', 'Small', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w8r@gm546ail.com', null, 15, 'WK1568215814');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jon', 'Snow','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'large+65ho456use@yahoo.com', '775214511', 16, 'MJ158613215');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jane', 'Carlson', '$2a$10$.UpeA8GIiDjd3U9j8Xxk8u.ZSeJgabSiiuOc9dtmbh3wtXEwpU/9.' , 'calami456ty.jane@gmail.com', '773224412', 17, 'CJ12542616');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jack', 'Vrátný', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.whi456te@gmail.com', '602125413', 18, 'WJ156781617');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Alice', 'Opastard','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'mali456ce@hot456mail.com', '603125714', 19, 'MA21586218');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Michal', 'Lalák', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w8r@456gmail.com', null, 20, 'WK1568215819');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Michael', 'Ap','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'large45house@yahoo.com', '775214515', 21, 'MJ158613220');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Mike', 'Mull', '$2a$10$.UpeA8GIiDjd3U9j8Xxk8u.ZSeJgabSiiuOc9dtmbh3wtXEwpU/9.' , 'calam45ity.jane@gmail.com', '773224416', 22, 'CJ12542621');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Mich', 'Big', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.whi65te@gmail.com', '602125417', 23, 'WJ156781622');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Mull', 'White','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'malic45e@hotmail.com', '603125718', 24, 'MA21586223');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Ell', 'Black', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w8r@65gmail.com', null, 25, 'WK1568215824');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Joe', 'Veryli','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'largeh456ouse@yahoo.com', '775214519', 24, 'MJ158613226');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jane', 'Russian', '$2a$10$.UpeA8GIiDjd3U9j8Xxk8u.ZSeJgabSiiuOc9dtmbh3wtXEwpU/9.' , 'cala654mity.jane@gmail.com', '773224420', 23, 'CJ12542627');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Jack', 'Czech', '$2a$10$ifGLlPUbtP/IO63B95X1W.Mpb1xLwYJAhS4aOyp9.B/3jvcIdIHT2' ,'jack.whi456te@gmaifdl.com', '602125421', 22, 'WJ156781628');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Alice', 'May','$2a$10$8pVZv5quuFodSskKMnil7u25Bd2okEwO1TFjqMPmQ/fRwxL9hZA3W' , 'malice64@hotmfdail.com', '603125722', 21, 'MA21586229');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Kate', 'June', '$2a$10$uMk/a7hIcr4SD1GpuRY8QuoZcLUxIa/HABVo/Ih9jzaocDoITfW.C' ,'k8w8r@fdgma456il.com', null, 20, 'WK1568215830');
INSERT INTO person VALUES (nextval('person_id_seq'), 'Joe', 'July','$2a$10$tzTB2RTSe.tOxZdaaWIzju.kC1ooDPcbPCgg8Jn0G9viE8yh9t2SO' , 'largeho645use@yfdahoo.com', '775214559', 19, 'MJ158613231');

INSERT INTO library VALUES (nextval('library_id_seq'), 'Prague NL', null, 1);
INSERT INTO library VALUES (nextval('library_id_seq'), 'Bechyne Library', null, 2);
INSERT INTO library VALUES (nextval('library_id_seq'), 'Tabor Library', 'central', 3);
INSERT INTO library VALUES (nextval('library_id_seq'), 'Sezimovo usti', 'belongs to Tabor', 4);
INSERT INTO library VALUES (nextval('library_id_seq'), 'Prague Mala Strana', 'part of NL', 1);

INSERT INTO genre VALUES (nextval('genre_id_seq'), 'fantasy');
INSERT INTO genre VALUES (nextval('genre_id_seq'), 'thriller');
INSERT INTO genre VALUES (nextval('genre_id_seq'), 'children');
INSERT INTO genre VALUES (nextval('genre_id_seq'), 'horror');
INSERT INTO genre VALUES (nextval('genre_id_seq'), 'adventure');

INSERT INTO employee VALUES (nextval('employee_id_seq'), '2016-01-01', '2017-02-28', 'librarian', 1, 1);
INSERT INTO employee VALUES (nextval('employee_id_seq'), '2017-03-01', null, 'head librarian', 1, 1);
INSERT INTO employee VALUES (nextval('employee_id_seq'), '2015-10-01', null, 'librarian', 4, 3);
INSERT INTO employee VALUES (nextval('employee_id_seq'), '2016-04-15', '2017-01-31', 'librarian', 2, 2);
INSERT INTO employee VALUES (nextval('employee_id_seq'), '2016-05-01', null, 'assistant librarian', 3, 4);

INSERT INTO book VALUES (nextval('book_id_seq'), 'For Whom the Bell Tolls', 1942, 'Penguin Books', 212, 'English', '978-0684803357', 1);
INSERT INTO book VALUES (nextval('book_id_seq'), 'For Whom the Bell Tolls', 1940, 'Harper Collins', 212, 'English', '978-0684803357', 1);
INSERT INTO book VALUES (nextval('book_id_seq'), 'Coraline', 2002, 'AbeBooks', 98, 'English', '978-0061649691', 3);
INSERT INTO book VALUES (nextval('book_id_seq'), 'Neverwhere', 1996, 'AbeBooks', 212, 'English', '978-0062371058', 4);
INSERT INTO book VALUES (nextval('book_id_seq'), 'Die Arena', 2009, 'Heyne Verlag', 212, 'German', '978-3453435230', 2);

INSERT INTO author VALUES (nextval('author_id_seq'), 'Ernest', 'Hemingway');
INSERT INTO author VALUES (nextval('author_id_seq'), 'Neil', 'Gaiman');
INSERT INTO author VALUES (nextval('author_id_seq'), 'Stephen', 'King');
INSERT INTO author VALUES (nextval('author_id_seq'), 'J. K.', 'Rowling');
INSERT INTO author VALUES (nextval('author_id_seq'), 'Robin', 'Hobb');

INSERT INTO booksbygenre VALUES (nextval('booksbygenre_id_seq'), 1, 3);
INSERT INTO booksbygenre VALUES (nextval('booksbygenre_id_seq'), 3, 3);
INSERT INTO booksbygenre VALUES (nextval('booksbygenre_id_seq'), 4, 3);
INSERT INTO booksbygenre VALUES (nextval('booksbygenre_id_seq'), 2, 5);
INSERT INTO booksbygenre VALUES (nextval('booksbygenre_id_seq'), 1, 4);

INSERT INTO booksbyauthor VALUES (1, 1, nextval('booksbyauthor_id_seq'));
INSERT INTO booksbyauthor VALUES (1, 2, nextval('booksbyauthor_id_seq'));
INSERT INTO booksbyauthor VALUES (3, 5, nextval('booksbyauthor_id_seq'));
INSERT INTO booksbyauthor VALUES (2, 3, nextval('booksbyauthor_id_seq'));
INSERT INTO booksbyauthor VALUES (2, 4, nextval('booksbyauthor_id_seq'));


INSERT INTO reservation VALUES (nextval('reservation_id_seq'), 1, 1, '2017-05-06','2017-05-07',false);
INSERT INTO reservation VALUES (nextval('reservation_id_seq'), 1, 2, '2017-05-08','2017-05-11',false);
INSERT INTO reservation VALUES (nextval('reservation_id_seq'), 1, 2, '2017-05-12','2017-05-18',false);
INSERT INTO reservation VALUES (nextval('reservation_id_seq'), 1, 2, '2017-05-19','2017-05-20',false);
INSERT INTO reservation VALUES (nextval('reservation_id_seq'), 4, 2, '2017-05-22','2017-05-29',false);