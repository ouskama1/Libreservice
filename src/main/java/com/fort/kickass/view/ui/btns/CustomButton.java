package com.fort.kickass.view.ui.btns;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.effect.Reflection;
import javafx.scene.input.MouseEvent;

/**
 * Custom button using pictures with three styles - Default, Active and Selected - that change on mouse hover and click.
 */
public class CustomButton extends Button {

    private String defaultStyle;
    private String activeStyle;
    private String selectedStyle;

    public CustomButton(double width, double height) {
        setPrefSize(width, height);
        reflect(true);
    }

    public CustomButton(double width, double height, boolean reflection) {
        setPrefSize(width, height);
        reflect(reflection);
    }

    public void reflect(boolean reflection) {
        if (reflection) {
            setEffect(new Reflection());
        }
    }

    public void initMouseHandlers() {
        addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setActiveStyle();
                    }
                });
        addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setDefaultStyle();
                    }
                });
        addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setSelectedStyle();
                    }
                });
        addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setActiveStyle();
                    }
                });
    }

    public void initStyles(String defaultStyle, String activeStyle, String selectedStyle) {
        this.defaultStyle = defaultStyle;
        this.activeStyle = activeStyle;
        this.selectedStyle = selectedStyle;
    }

    public void setDefaultStyle() {
        setStyle(defaultStyle);
    }

    public void setActiveStyle() {
        setStyle(activeStyle);
    }

    public void setSelectedStyle() {
        setStyle(selectedStyle);
    }


}
