package com.fort.kickass.view.ui.pane;

import com.fort.kickass.view.ui.base.Const;
import com.fort.kickass.view.ui.js.Renderer;
import com.fort.kickass.view.ui.js.Wrapper;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The panel that contains the Architecture manual.
 */
public class ArchitecturePane extends BorderPane {

    public ArchitecturePane() {
        init();
    }

    /**
     * Sets panel's initial settings.
     */
    private void init() {
        setLoading();
        final WebEngine[] eng = {null};
        WebView webview = new WebView();

        Task task = new Task<String>() {
            @Override
            public String call() {
                Wrapper wrapper = new Wrapper();
                Renderer renderer = wrapper.getRenderer();
                InputStreamReader stream = new InputStreamReader(getClass().getResourceAsStream(Const.ARCHITECTURE_MARKDOWN));
                Scanner s = new Scanner(stream).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";

                String html = renderer.render(result);
                webview.setVisible(true);

                String pattern = "<img src=\"(.*?)\"";

                // Create a Pattern object
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(html);

                HashMap<String, String> matches = new HashMap();

                while (m.find()) {
                    String key = m.group(1);
                    String value = getClass().getResource(m.group(1)).getFile();
                    matches.put(key, value);
                }

                Iterator it = matches.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    html = html.replaceAll((String) pair.getKey(), "file:" + pair.getValue());
                }

                return html;
            }
        };

        task.setOnSucceeded((Event event) -> {
            setStyle(Const.MANUALPANES_BACKGROUND);
            eng[0] = webview.getEngine();

            WorkerStateEvent e = (WorkerStateEvent) event;
            final String html = (String) e.getSource().getValue();
            if (html != null) {
                try {
                    eng[0].loadContent(html, "text/html");
                    setCenter(webview);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "exception");
                }
            }
            eng[0].setUserStyleSheetLocation(getClass().getResource("/man/css/style.css").toString());
        });
        new Thread(task).start();
    }

    /**
     * Sets the loading image.
     */
    private void setLoading() {
        HBox hBox = new HBox();
        // load the image
        Image image = new Image(Const.LOADING_GIF);
        ImageView iv1 = new ImageView();
        iv1.setImage(image);
        hBox.getChildren().addAll(iv1);
        setCenter(hBox);
    }
}
