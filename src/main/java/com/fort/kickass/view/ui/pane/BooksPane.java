package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.view.dto.*;
import com.fort.kickass.view.ui.base.Const;
import com.fort.kickass.view.ui.btns.AddBookBtn;
import com.fort.kickass.view.ui.btns.DetailBtn;
import com.fort.kickass.view.ui.btns.EditBookBtn;
import com.google.inject.Injector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.util.List;

/**
 * The main books panel.
 */
public class BooksPane extends BorderPane {

    private Injector injector;
    private TableView bookTable;
    private TableView bookDetailTable;
    private BookSimpleTableDto selectedBook;
    private CatalogPane catalogPane;
    private BookService bookService;
    private MainPane mainPane;
    private List<AuthorDto> selectedAuthors;
    private List<GenreDto> selectedGenres;

    public BooksPane(CatalogPane catalogPane, Injector injector) {
        this.catalogPane = catalogPane;
        this.injector = injector;
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        bookService = injector.getInstance(BookService.class);

        Pane topSpace = new Pane();
        topSpace.setPrefSize(0, Const.CATALOG_PANE_TOP_PADDING);

        DetailBtn detailBtn = new DetailBtn(this);
        AddBookBtn addBookBtn = new AddBookBtn(this, injector);
        EditBookBtn editBookBtn = new EditBookBtn(this, injector);
        HBox hBox = new HBox(5);
        hBox.setAlignment(Pos.BOTTOM_RIGHT);
        hBox.getChildren().addAll(editBookBtn, addBookBtn, detailBtn);

        bookTable = new TableView();
        bookTable.setStyle(Const.CATALOG_PANE_SIMPLE_TABLE_STYLE);
        TableColumn title = new TableColumn("Title");
        title.setMinWidth(200);
        title.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, String>("title")
        );

        TableColumn author = new TableColumn("Author");
        author.setMinWidth(200);
        author.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, String>("author")
        );

        bookTable.getColumns().addAll(title, author);
        bookTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        bookTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                selectedBook = (BookSimpleTableDto) bookTable.getSelectionModel().getSelectedItem();
                System.out.println("Selected: Title: " + selectedBook.getTitle() + " , " + "Author: " + selectedBook.getAuthor());
                showSelectedView();
            }
        });


        bookDetailTable = new TableView();
        bookDetailTable.setStyle(Const.CATALOG_PANE_DETAILED_TABLE_STYLE);
        TableColumn publisher = new TableColumn("Publisher");
        publisher.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, String>("publisher")
        );
        TableColumn publishedYear = new TableColumn("Year");
        publishedYear.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, Integer>("publishedYear")
        );
        TableColumn language = new TableColumn("Language");
        language.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, String>("language")
        );
        TableColumn thisLibrary = new TableColumn("In this library?");
        thisLibrary.setCellValueFactory(
                new PropertyValueFactory<BookSimpleTableDto, Boolean>("inThisLibrary")
        );

        bookDetailTable.getColumns().addAll(publisher, publishedYear, language, thisLibrary);
        bookDetailTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        bookDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                selectedBook.setSelectedDetail((BookDetailedTableDto) bookDetailTable.getSelectionModel().getSelectedItem());
                detailBtn.setDisable(false);
                editBookBtn.setDisable(false);
            }
        });

        setBottom(hBox);
        setTop(topSpace);
        setLeft(bookTable);
        setCenter(bookDetailTable);


        // Tohle potom smazat
        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(bookService.getAllBooks());
        bookTable.setItems(selectedResult);

    }

    /**
     * Shows the selected books.
     */
    public void showSelectedView() {
        ObservableList<BookDetailedTableDto> selectedResult = FXCollections.observableArrayList(selectedBook.getDetailedResults());
        bookDetailTable.setItems(selectedResult);
    }

    /**
     * Goes to the selected book's details.
     */
    public void goToDetail() {
        catalogPane.viewDetail(selectedBook);
    }

    /**
     * Refreshes the panel.
     */
    public void refresh() {
        catalogPane.refresh();
    }

    /**
     * Open the panel for Adding a new book.
     */
    public void addBook() {
        setLeft(null);
        setCenter(new AddBookCenterPane(injector, this));
    }

    /**
     * Opens the panel for editing the selected book.
     */
    public void editBook() {
        setLeft(null);
        setCenter(new EditBookCenterPane(injector, this, selectedBook));
    }

    public List<AuthorDto> getSelectedAuthors() {
        return selectedAuthors;
    }

    public void setSelectedAuthors(List<AuthorDto> selectedAuthors) {
        this.selectedAuthors = selectedAuthors;
    }

    public List<GenreDto> getSelectedGenres() {
        return selectedGenres;
    }

    public void setSelectedGenres(List<GenreDto> selectedGenres) {
        this.selectedGenres = selectedGenres;
    }
}
