package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.ui.base.Const;
import com.fort.kickass.view.ui.pane.CalendarPane;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

/**
 * Created by kommar on 24/04/2017.
 */
public class CalendarBtn extends Button {

    private ReservationDto reservation;
    private CalendarPane calendarPane;
    private final int DEFAULT_BUTTON_RADIUS = 5;
    private final int ACTIVE_BUTTON_RADIUS = 30;
    private boolean pressed;
    private int day;

    public CalendarBtn(int day, ReservationDto reservation, CalendarPane calendarPane) {
        super(day + "");
        this.day = day;
        this.calendarPane = calendarPane;
        this.reservation = reservation;
        setPrefSize(Const.SCHEDULE_PANE_CALENDAR_BTN_SIZE, Const.SCHEDULE_PANE_CALENDAR_BTN_SIZE);
        setDefaultStyle();
        initMouseHandlers();
    }

    public void initMouseHandlers() {
        addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setActiveStyle();
                        pressed = false;
                        if (reservation != null) {
                            calendarPane.showReservation(reservation);
                        } else {
                            calendarPane.newReservationShow(day);
                        }
                    }
                });
        addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setDefaultStyle();
                        calendarPane.showSelectedPane();
                    }
                });
        addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setPressedStyle();
                        pressed = true;
                    }
                });
        addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        setActiveStyle();
                        if (pressed) {
                            calendarPane.selectPane();
                        }
                        if (reservation != null) {
                            calendarPane.showReservation(reservation);
                        } else {
                            calendarPane.newReservation(day);
                        }
                    }
                });
    }

    private void setDefaultStyle() {
        if (reservation == null) {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(64, 33, 250, 0.17);" +
                    " -fx-border-radius: " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + ";");
        } else {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(250, 128, 114, 0.26);" +
                    " -fx-border-radius: " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + " " + DEFAULT_BUTTON_RADIUS + ";");
        }
    }

    private void setActiveStyle() {
        if (reservation == null) {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(89, 232, 88, 0.44);" +
                    " -fx-border-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";");
        } else {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(233, 150, 122, 0.6);" +
                    " -fx-border-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";");
        }
    }

    private void setPressedStyle() {
        if (reservation == null) {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(72, 250, 18, 0.74);" +
                    " -fx-border-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";");
        } else {
            setStyle("-fx-font-size: 100%;" +
                    " -fx-font-family: Verdana, Geneva, sans-serif;" +
                    " -fx-background-color: rgba(250, 128, 114, 0.69);" +
                    " -fx-border-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";" +
                    " -fx-background-radius: " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + " " + ACTIVE_BUTTON_RADIUS + ";");
        }
    }


}
