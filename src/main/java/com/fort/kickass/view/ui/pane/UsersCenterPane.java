package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.dto.UserReservationDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;
import com.fort.kickass.view.ui.btns.CheckOutBtn;
import com.fort.kickass.view.ui.btns.DeleteBtn;
import com.google.inject.Injector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The main Users panel.
 */
public class UsersCenterPane extends BorderPane {

    private UsersService usersService;
    private Injector injector;
    private UsersDto usersDto;
    private UsersPane usersPane;
    private String labelNameStyle = "    -fx-font-size: 14px;\n" +
            "    -fx-text-fill: #333333;\n" +
            "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1);";
    private String labelDetailStyle = "    -fx-font-size: 14px;\n" +
            "    -fx-font-style: italic;\n" +
            "    -fx-text-fill: #823439;\n" +
            "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1);";
    private String mainTextsStyle = "    -fx-font-size: 20px;\n" +
            "    -fx-font-family: \"Arial Black\";\n" +
            "    -fx-fill: #818181;\n" +
            "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );";

    public UsersCenterPane(Injector injector, UsersDto usersDto, UsersPane usersPane) {
        this.injector = injector;
        this.usersDto = usersDto;
        this.usersPane = usersPane;
        usersService = injector.getInstance(UsersService.class);
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        initTop();
        initLeft();
        initCenter();
        initRight();
        initBottom();
    }

    /**
     * Sets the top panel.
     */
    private void initTop() {
        VBox topLayer = new VBox();
        Pane topSpacer = new Pane();
        topSpacer.setPrefHeight(100);
        topLayer.getChildren().addAll(topSpacer);
        setTop(topLayer);
    }

    /**
     * Sets the left panel.
     */
    private void initLeft() {
        HBox leftLayer = new HBox();
        Pane leftSpacer = new Pane();
        leftSpacer.setPrefWidth(150);
        leftLayer.getChildren().addAll(leftSpacer);
        setLeft(leftLayer);
    }

    /**
     * Sets the center panel.
     */
    private void initCenter() {
        Text personalInfoHeader = new Text("Personal Information");
        personalInfoHeader.setStyle(mainTextsStyle);

        Text addressHeader = new Text("Address");
        addressHeader.setStyle(mainTextsStyle);

        Label firstNameLabel = new Label("First name: ");
        firstNameLabel.setStyle(labelNameStyle);
        Label firstName = new Label(usersDto.getFirstName());
        firstName.setStyle(labelDetailStyle);
        Label lastNameLabel = new Label("Last name: ");
        lastNameLabel.setStyle(labelNameStyle);
        Label lastName = new Label(usersDto.getLastName());
        lastName.setStyle(labelDetailStyle);
        Label emailLabel = new Label("Email: ");
        emailLabel.setStyle(labelNameStyle);
        Label email = new Label(usersDto.getEmail());
        email.setStyle(labelDetailStyle);
        Label phoneLabel = new Label("Phone: ");
        phoneLabel.setStyle(labelNameStyle);
        Label phone = new Label(usersDto.getPhone());
        phone.setStyle(labelDetailStyle);
        Label cardLabel = new Label("Card number: ");
        cardLabel.setStyle(labelNameStyle);
        Label card = new Label(usersDto.getCardNumber());
        card.setStyle(labelDetailStyle);

        HBox firstNameBox = new HBox();
        firstNameBox.getChildren().addAll(firstNameLabel, firstName);
        HBox lastNameBox = new HBox();
        lastNameBox.getChildren().addAll(lastNameLabel, lastName);
        HBox emailBox = new HBox();
        emailBox.getChildren().addAll(emailLabel, email);
        HBox phoneBox = new HBox();
        phoneBox.getChildren().addAll(phoneLabel, phone);
        HBox cardBox = new HBox();
        cardBox.getChildren().addAll(cardLabel, card);

        VBox personalDetails = new VBox();
        personalDetails.getChildren().addAll(personalInfoHeader, firstNameBox, lastNameBox, emailBox, phoneBox, cardBox);

        Label streetLabel = new Label("Street: ");
        streetLabel.setStyle(labelNameStyle);
        Label street = new Label(usersDto.getAddress().getStreet());
        street.setStyle(labelDetailStyle);
        Label houseNoLabel = new Label("House number: ");
        houseNoLabel.setStyle(labelNameStyle);
        Label houseNo = new Label();
        houseNo.setText(Integer.toString(usersDto.getAddress().getHouseNumber()));
        houseNo.setStyle(labelDetailStyle);
        Label cityLabel = new Label("City: ");
        cityLabel.setStyle(labelNameStyle);
        Label city = new Label(usersDto.getAddress().getCity());
        city.setStyle(labelDetailStyle);
        Label zipcodeLabel = new Label("Zipcode: ");
        zipcodeLabel.setStyle(labelNameStyle);
        Label zipcode = new Label(usersDto.getAddress().getZipcode());
        zipcode.setStyle(labelDetailStyle);
        Label countryLabel = new Label("Country: ");
        countryLabel.setStyle(labelNameStyle);
        Label country = new Label(usersDto.getAddress().getCountry());
        country.setStyle(labelDetailStyle);
        Label otherDetailsLabel = new Label("Other details: ");
        otherDetailsLabel.setStyle(labelNameStyle);
        Label otherDetails = new Label(usersDto.getAddress().getOtherDetails());
        otherDetails.setStyle(labelDetailStyle);

        HBox streetBox = new HBox();
        streetBox.getChildren().addAll(streetLabel, street);
        HBox houseNoBox = new HBox();
        houseNoBox.getChildren().addAll(houseNoLabel, houseNo);
        HBox cityBox = new HBox();
        cityBox.getChildren().addAll(cityLabel, city);
        HBox zipcodeBox = new HBox();
        zipcodeBox.getChildren().addAll(zipcodeLabel, zipcode);
        HBox countryBox = new HBox();
        countryBox.getChildren().addAll(countryLabel, country);
        HBox otherDetailsBox = new HBox();
        otherDetailsBox.getChildren().addAll(otherDetailsLabel, otherDetails);

        VBox address = new VBox();
        address.getChildren().addAll(addressHeader, streetBox, houseNoBox, cityBox, zipcodeBox, countryBox, otherDetailsBox);

        Pane detailSpacer = new Pane();
        detailSpacer.setPrefWidth(60);

        HBox userDetailsRow = new HBox();
        userDetailsRow.getChildren().addAll(personalDetails, detailSpacer, address);

        Pane middleSpacer = new Pane();
        middleSpacer.setPrefHeight(20);

        TableView reservationsTable = new TableView();
        reservationsTable.setStyle("-fx-background-color: rgba(179,48,185,0.15);");

        TableColumn authors = new TableColumn("Authors");
        authors.setCellValueFactory(new PropertyValueFactory<UserReservationDto, String>("authors"));

        TableColumn book = new TableColumn("Book");
        book.setCellValueFactory(new PropertyValueFactory<UserReservationDto, String>("bookTitle"));

        TableColumn dateFrom = new TableColumn("Date from");
        dateFrom.setCellValueFactory(new PropertyValueFactory<UserReservationDto, String>("dateFrom"));

        TableColumn dateTo = new TableColumn("Date to");
        dateTo.setCellValueFactory(new PropertyValueFactory<UserReservationDto, String>("dateTo"));

        TableColumn checkedOut = new TableColumn("Currently out");
        checkedOut.setCellValueFactory(new PropertyValueFactory<UserReservationDto, String>("checkedOut"));

        reservationsTable.getColumns().addAll(authors, book, dateFrom, dateTo, checkedOut);

        HBox buttons = new HBox();
        CheckOutBtn checkOutBtn = new CheckOutBtn(this, injector);
        DeleteBtn deleteBtn = new DeleteBtn(this, injector);
        buttons.getChildren().addAll(checkOutBtn, deleteBtn);

        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(usersDto.getReservations());
        reservationsTable.setItems(selectedResult);

        reservationsTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                UserReservationDto usersDto = (UserReservationDto) reservationsTable.getSelectionModel().getSelectedItem();
                checkOutBtn.setDisable(false);
                checkOutBtn.setSelectedReservation(usersDto);
                deleteBtn.setDisable(false);
                deleteBtn.setSelectedReservation(usersDto);
            }
        });
        VBox userDetails = new VBox();
        userDetails.getChildren().addAll(userDetailsRow, middleSpacer, reservationsTable, buttons);
        setCenter(userDetails);
    }

    /**
     * Sets the right panel.
     */
    private void initRight() {
        HBox rightLayer = new HBox();

        Pane rightSpacer = new Pane();
        rightSpacer.setPrefWidth(150);

        rightLayer.getChildren().addAll(rightSpacer);
        setRight(rightLayer);
    }

    /**
     * Sets the bottom panel.
     */
    private void initBottom() {

    }

    public void refresh() {
        usersPane.refresh(usersDto.getCardNumber());
    }
}
