package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.LoginPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Marcel on 25.03.2017.
 */
public class LoginBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/loginBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/loginBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/loginBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    public LoginPane loginPane;

    public LoginBtn(LoginPane loginPane) {
        super(100, 50);
        this.loginPane = loginPane;
        init();
    }

    public LoginBtn(boolean reflection, LoginPane loginPane) {
        super(100, 50, reflection);
        this.loginPane = loginPane;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                loginPane.login();
            }
        });
    }


}
