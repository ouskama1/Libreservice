package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.UsersLeftPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 22.5.2017..
 */
public class SearchButton extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/searchBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/searchBtnSelected.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/searchBtnActive.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private UsersLeftPane usersLeftPane;

    public SearchButton(UsersLeftPane usersLeftPane) {
        super(75, 35);
        this.usersLeftPane = usersLeftPane;
        setDisable(false);
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                usersLeftPane.search();
            }
        });
    }
}
