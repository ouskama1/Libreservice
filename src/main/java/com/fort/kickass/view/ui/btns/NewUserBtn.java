package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.dto.UserReservationDto;
import com.fort.kickass.view.ui.pane.UsersLeftPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class NewUserBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/newUserBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/newUserBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/newUserBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private Injector injector;
    private UserReservationDto userReservationDto;

    private UsersLeftPane usersLeftPane;

    public NewUserBtn(UsersLeftPane usersLeftPane, Injector injector) {
        super(100, 50);
        this.usersLeftPane = usersLeftPane;
        this.injector = injector;
        init();
    }

    public NewUserBtn(boolean reflection, UsersLeftPane usersLeftPane, Injector injector) {
        super(100, 50, reflection);
        this.usersLeftPane = usersLeftPane;
        this.injector = injector;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                usersLeftPane.newUser();
            }
        });
    }

    public void setSelectedReservation(UserReservationDto userReservationDto) {
        this.userReservationDto = userReservationDto;

    }
}
