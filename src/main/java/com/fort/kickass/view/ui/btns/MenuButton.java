package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.base.Const;

public class MenuButton extends CustomButton {
    public MenuButton() {
        super(Const.MENU_BTN_SIZE.getWidth(), Const.MENU_BTN_SIZE.getHeight());
    }
}
