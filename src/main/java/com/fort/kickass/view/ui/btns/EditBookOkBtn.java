package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.EditBookLeftPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by kommar on 29/05/2017.
 */
public class EditBookOkBtn extends CustomButton {
    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/okBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/okBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/okBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private Injector injector;

    EditBookLeftPane editBookLeftPane;

    public EditBookOkBtn(EditBookLeftPane editBookLeftPane, Injector injector) {
        super(100, 50);
        this.editBookLeftPane = editBookLeftPane;
        this.injector = injector;
        init();
    }

    public EditBookOkBtn(boolean reflection, EditBookLeftPane editBookLeftPane, Injector injector) {
        super(100, 50, reflection);
        this.editBookLeftPane = editBookLeftPane;
        this.injector = injector;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                editBookLeftPane.editBook();
            }
        });
    }
}
