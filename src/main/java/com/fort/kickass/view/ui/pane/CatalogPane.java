package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.BookSimpleTableDto;
import com.google.inject.Injector;
import javafx.scene.layout.BorderPane;

/**
 * The main catalog panel.
 */
public class CatalogPane extends BorderPane {

    private Injector injector;
    BooksPane booksPane;
    BookDetailPane bookDetailPane;
    protected BookService bookService;
    private MainPane mainPane;

    public CatalogPane(Injector injector, MainPane mainPane) {
        this.mainPane = mainPane;
        this.injector = injector;
        booksPane = new BooksPane(this, injector);
        bookService = injector.getInstance(BookService.class);
        init();
    }

    /**
     * Sets the initial panel's settings.
     */
    private void init() {
        setCenter(booksPane);
    }

    public void viewBooks() {
        setCenter(booksPane);
    }

    /**
     * Shows the details for the selected book.
     *
     * @param selectedSimpleBook
     */
    public void viewDetail(BookSimpleTableDto selectedSimpleBook) {
        BookDetailDto book = bookService.getBook(selectedSimpleBook.getSelectedDetail().getId());
        bookDetailPane = new BookDetailPane(book, injector, mainPane);
        setCenter(bookDetailPane);
    }

    public void refresh() {
        mainPane.goToCatalog();
    }
}
