package com.fort.kickass.view.ui.pane;

import com.fort.kickass.view.dto.UsersDto;
import com.google.inject.Injector;
import javafx.scene.layout.BorderPane;

/**
 * The main users panel.
 */
public class UsersPane extends BorderPane {

    private Injector injector;
    private MainPane mainPane;
    private String userSearch;

    public UsersPane(Injector injector, MainPane mainPane) {
        this.injector = injector;
        this.mainPane = mainPane;
        init();
    }

    public UsersPane(Injector injector, String user, MainPane mainPane) {
        this.injector = injector;
        this.mainPane = mainPane;
        this.userSearch = user;
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        initLeft();
    }

    private void initLeft() {
        setLeft(new UsersLeftPane(this, userSearch, injector));
    }

    public void showUserInfo(UsersDto usersDto) {
        setCenter(new UsersCenterPane(injector, usersDto, this));
    }

    public void refresh(String user) {
        mainPane.goToUsersPane(user);
    }

    public void newUser() {
        setCenter(new NewUserCenterPane(injector, this));
    }
}
