package com.fort.kickass.view.ui.pane;

import com.fort.kickass.base.Utils;
import com.fort.kickass.service.impl.ReservationService;
import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;
import com.fort.kickass.view.ui.btns.NewReservationOkBtn;
import com.google.inject.Injector;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The right-side Reservations panel.
 */
public class BookReservationPane extends BorderPane {

    private final static Logger LOGGER = Logger.getLogger(BookReservationPane.class.getName());
    String headingStyle = " -fx-font-family: \"Arial Black\", Gadget, sans-serif;";
    private VBox selectedPane;
    private VBox visiblePane;
    private MainPane mainPane;
    private Injector injector;
    private String selectedDate1;
    private String selectedDate2;
    private String selectedCardNumber;
    private UsersService usersService;
    private ReservationService reservationService;
    private BookDetailDto book;
    private Text errorMessage;
    private BookDetailPane bookDetailPane;

    public BookReservationPane(MainPane mainPane, Injector injector, BookDetailDto book, BookDetailPane bookDetailPane) {
        this.mainPane = mainPane;
        this.injector = injector;
        this.bookDetailPane = bookDetailPane;
        usersService = injector.getInstance(UsersService.class);
        reservationService = injector.getInstance(ReservationService.class);
        this.book = book;
        errorMessage = new Text();
        errorMessage.setStyle("    -fx-fill: firebrick;\n" +
                "    -fx-font-weight: bold;\n" +
                "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1 );");
        init();
    }

    /**
     * Sets panel's initial settings.
     */
    private void init() {
        setPrefWidth(400);
        setPrefHeight(200);
        setStyle("-fx-background-color: rgba(47, 64, 139, 0.42);" +
                " -fx-border-radius: 80 80 80 80;" +
                " -fx-background-radius: 80 80 80 80;");
        Pane topSpacer = new Pane();
        topSpacer.setPrefHeight(100);
        Pane leftSpacer = new Pane();
        leftSpacer.setPrefWidth(100);
        setTop(topSpacer);
        setLeft(leftSpacer);
        selectedPane = new VBox();
    }

    public void showSelectedPane() {
        setCenter(selectedPane);
    }

    public void selectPane() {
        selectedPane = visiblePane;
    }

    /**
     * Shows the chosen Reservation on the reservation panel.
     *
     * @param reservation
     */
    public void showReservation(ReservationDto reservation) {
        GridPane reservationDetailPane = new GridPane();
        Label heading = new Label("Already reserved");
        heading.setStyle(headingStyle);
        Label labelUser = new Label("User   ");
        Label labelFrom = new Label("From   ");
        Label labelTo = new Label("To   ");
        Label labelCheckedOut = new Label("Checked Out   ");

        reservationDetailPane.setHalignment(labelUser, HPos.RIGHT);
        reservationDetailPane.setHalignment(labelFrom, HPos.RIGHT);
        reservationDetailPane.setHalignment(labelTo, HPos.RIGHT);
        reservationDetailPane.setHalignment(labelCheckedOut, HPos.RIGHT);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Hyperlink link = new Hyperlink();
        link.setStyle("-fx-font-family: \"Arial Black\", Gadget, sans-serif;" +
                "-fx-text-fill: rgba(250, 235, 215, 0.56);");
        link.setText(reservation.getPerson().getFirstName() + " " + reservation.getPerson().getLastName());
        link.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mainPane.goToUsersPane(reservation.getPerson().getCardNumber());
            }
        });

        reservationDetailPane.add(labelUser, 1, 10);
        reservationDetailPane.add(link, 2, 10);
        reservationDetailPane.add(labelFrom, 1, 11);
        reservationDetailPane.add(new Label(format.format(reservation.getDateFrom().getTime())), 2, 11);
        reservationDetailPane.add(labelTo, 1, 12);
        reservationDetailPane.add(new Label(format.format(reservation.getExpectedDateTo().getTime())), 2, 12);
        reservationDetailPane.add(labelCheckedOut, 1, 13);
        reservationDetailPane.add(new Label(reservation.isCheckedOut() + ""), 2, 13);
        setBottom(reservationDetailPane);
        VBox content = new VBox(20);
        content.getChildren().addAll(heading, reservationDetailPane);
        visiblePane = content;
        setCenter(visiblePane);
    }

    /**
     * Panel for creating a new Reservation.
     *
     * @param date1
     * @param date2
     */
    public void newReservation(String date1, String date2) {
        GridPane reservationDetailPane = new GridPane();
        Label heading = new Label("New Reservation");
        heading.setStyle(headingStyle);
        Label labelUser = new Label("User Card number  ");
        Label labelFrom = new Label("From   ");
        Label labelTo = new Label("To   ");

        reservationDetailPane.setHalignment(labelUser, HPos.RIGHT);
        reservationDetailPane.setHalignment(labelFrom, HPos.RIGHT);
        reservationDetailPane.setHalignment(labelTo, HPos.RIGHT);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        NewReservationOkBtn okBtn = new NewReservationOkBtn(this);

        TextField cardNumber = new TextField();
        cardNumber.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                if (!newValue.isEmpty() && !date1.isEmpty() && !date2.isEmpty()) {
                    okBtn.setDisable(false);
                    selectedCardNumber = newValue;
                    selectedDate1 = date1;
                    selectedDate2 = date2;
                } else {
                    okBtn.setDisable(true);
                }
            }
        });

        reservationDetailPane.add(labelUser, 1, 10);
        reservationDetailPane.add(cardNumber, 2, 10);
        reservationDetailPane.add(labelFrom, 1, 11);
        reservationDetailPane.add(new Label(date1), 2, 11);
        reservationDetailPane.add(labelTo, 1, 12);
        reservationDetailPane.add(new Label(date2), 2, 12);
        setBottom(reservationDetailPane);
        VBox content = new VBox(20);
        content.getChildren().addAll(heading, reservationDetailPane, okBtn, errorMessage);
        visiblePane = content;
        setCenter(visiblePane);
    }

    /**
     * Creates a reservation.
     */
    public void createReservation() {
        ReservationDto dto = new ReservationDto();
        dto.setCheckedOut(false);
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        try {
            dto.setDateFrom(Utils.dateToCalendar(format.parse(selectedDate1)));
            dto.setExpectedDateTo(Utils.dateToCalendar(format.parse(selectedDate2)));
        } catch (ParseException ex) {
            LOGGER.log(Level.WARNING, "Could not parse the given date");
            LOGGER.log(Level.ALL, ex.getMessage());
            return;
        }
        dto.setBook(book);
        UsersDto user = (UsersDto) usersService.getUserWithCardNumber(selectedCardNumber);
        if (user == null) {
            errorMessage.setText("User doesnt exist");
            LOGGER.log(Level.WARNING, "User doesnt exist");
            return;
        }
        dto.setPerson(user);
        List<ViewDto> reservationInbetween = reservationService.getReservationsFromTo(dto.getDateFrom(), dto.getExpectedDateTo(), book.getId());
        if (!reservationInbetween.isEmpty()) {
            errorMessage.setText("Inbetween dates you chose is already reservation(s)");
            LOGGER.log(Level.WARNING, "Inbetween dates you chose is already reservation(s)");
            return;
        }
        reservationService.createReservation(dto);
        bookDetailPane.refreshMonth();
    }
}
