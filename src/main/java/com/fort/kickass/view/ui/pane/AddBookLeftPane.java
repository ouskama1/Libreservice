package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.ui.btns.AddBookOkBtn;
import com.google.inject.Injector;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * The left-side panel for Adding new books, allows adding Book information.
 */
public class AddBookLeftPane extends GridPane {

    private Injector injector;
    private BooksPane booksPane;
    private BookService bookService;

    private String mainTextsStyle = "    -fx-font-size: 32px;\n" +
            "    -fx-font-family: \"Arial Black\";\n" +
            "    -fx-fill: #818181;\n" +
            "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );";

    private TextField bookTitleField;
    private TextField publishedYearField;
    private TextField publisherField;
    private TextField pagesField;
    private TextField languageField;
    private TextField isbnField;

    private Text formValidationMessage;

    public AddBookLeftPane(Injector injector, BooksPane booksPane) {
        this.injector = injector;
        this.booksPane = booksPane;
        bookService = injector.getInstance(BookService.class);
        init();
    }

    /**
     * Sets panel's initial settings.
     */
    private void init() {
        setAlignment(Pos.TOP_CENTER);
        setHgap(10);
        setVgap(10);
        setPadding(new Insets(25, 25, 25, 25));
        String leftSpace = "         ";

        Text bookInfo = new Text("Book Information");
        bookInfo.setStyle(mainTextsStyle);

        Text address = new Text("Address");
        address.setStyle(mainTextsStyle);

        Label bookTitle = new Label(leftSpace + "Title:  ");
        Label publishedYear = new Label(leftSpace + "Year:  ");
        Label publisher = new Label(leftSpace + "Publisher:  ");
        Label pages = new Label(leftSpace + "Pages:  ");
        Label language = new Label(leftSpace + "Language:  ");
        Label isbn = new Label(leftSpace + "ISBN:  ");

        bookTitleField = new TextField();
        publishedYearField = new TextField();
        publisherField = new TextField();
        pagesField = new TextField();
        languageField = new TextField();
        isbnField = new TextField();


        Text inThisLibraryText = new Text("In this library?");
        inThisLibraryText.setStyle("-fx-font-size: 15px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-fill: #3f8156;\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");

        add(bookInfo, 0, 1, 2, 1);
        add(bookTitle, 0, 2);
        add(publishedYear, 0, 3);
        add(publisher, 0, 4);
        add(pages, 0, 5);
        add(language, 0, 6);
        add(isbn, 0, 7);

        add(bookTitleField, 1, 2);
        add(publishedYearField, 1, 3);
        add(publisherField, 1, 4);
        add(pagesField, 1, 5);
        add(languageField, 1, 6);
        add(isbnField, 1, 7);

        AddBookOkBtn okBtn = new AddBookOkBtn(this, injector);
        add(okBtn, 1, 15);
        GridPane.setHalignment(okBtn, HPos.RIGHT);

        formValidationMessage = new Text();
        add(formValidationMessage, 2, 15);
        formValidationMessage.setStyle("    -fx-fill: firebrick;\n" +
                "    -fx-font-weight: bold;\n" +
                "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1 );");

        Pane rightTopSpacer = new Pane();
        rightTopSpacer.setPrefHeight(30);
        add(rightTopSpacer, 5, 0);
    }

    /**
     * Sets the details given by the user into a dto and saves the book.
     */
    public void createBook() {
        if (!isValid()) {
            return;
        }

        BookDetailDto dto = new BookDetailDto();
        dto.setTitle(bookTitleField.getText());
        int year = Integer.parseInt(publishedYearField.getText());
        dto.setPublishedYear(year);
        dto.setPublisher(publisherField.getText());
        int pages = Integer.parseInt(pagesField.getText());
        dto.setPages(pages);
        dto.setLanguage(languageField.getText());
        dto.setIsbn(isbnField.getText());
        dto.setAuthors(booksPane.getSelectedAuthors());
        dto.setGenres(booksPane.getSelectedGenres());
        bookService.createBook(dto);

        booksPane.refresh();
    }

    /**
     * Validates user input.
     *
     * @return boolean
     */
    public boolean isValid() {
        if (bookTitleField.getText().isEmpty()) {
            formValidationMessage.setText("Title field needs to be filled up");
            return false;
        } else if (publishedYearField.getText().isEmpty()) {
            formValidationMessage.setText("Year field needs to be filled up");
            return false;
        } else if (!isParsableToInt(publishedYearField.getText())) {
            formValidationMessage.setText("Year field needs to be a number");
            return false;
        } else if (publisherField.getText().isEmpty()) {
            formValidationMessage.setText("Publisher field needs to be filled up");
            return false;
        } else if (pagesField.getText().isEmpty()) {
            formValidationMessage.setText("Pages field needs to be filled up");
            return false;
        } else if (!isParsableToInt(pagesField.getText())) {
            formValidationMessage.setText("Pages field needs to be a number");
            return false;
        } else if (languageField.getText().isEmpty()) {
            formValidationMessage.setText("Language field needs to be filled up");
            return false;
        } else if (isbnField.getText().isEmpty()) {
            formValidationMessage.setText("ISBN field needs to be filled up");
            return false;
        } else {
            return true;
        }
    }

    public boolean isParsableToInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}