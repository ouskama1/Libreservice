package com.fort.kickass.view.ui.base;

import java.awt.*;

/**
 * Created by Marcel on 25.03.2017.
 */
public class Const {
    // General
    public static final Dimension MAIN_FRAME_SIZE = new Dimension(1400, 900);
    public static final String MAIN_FRAME_TITLE = "Libreservice";
    public static final int TOP_MENU_SPACING = 5;

    // Buttons
    public static final Dimension MENU_BTN_SIZE = new Dimension(100, 50);

    // css
    public static final String LOGIN_SCENE_CSS = "/view/css/loginScene.css";
    public static final String HOME_SCENE_CSS = "/view/css/homeScene.css";

    //Catalog page
    public static final int CATALOG_PANE_TOP_PADDING = 50;
    public static final String CATALOG_PANE_SIMPLE_TABLE_STYLE = "-fx-background-color:rgba(7, 37, 51, 0.25)";
    public static final String CATALOG_PANE_DETAILED_TABLE_STYLE = "-fx-background-color:rgba(51, 20, 37, 0.28)";

    //Book Detail page
    public static final int BOOK_DETAIL_PANE_TOP_LAYER_HEIGHT = 50;
    public static final String BOOK_DETAIL_PANE_TABLE_STYLE = "-fx-background-color:rgba(7, 37, 51, 0.25)";
    public static final int BOOK_DETAIL_PANE_DETAILS_WIDTH = 500;
    public static final int RESERVATION_DETAIL_PANE_DETAILS_WIDTH = 500;
    public static final int SCHEDULE_PANE_CALENDAR_BTN_SIZE = 40;

    public static final String LOADING_GIF = "/view/images/loading.gif";
    public static final String USER_MANUAL_MARKDOWN = "/man/user-manual.md";
    public static final String ARCHITECTURE_MARKDOWN = "/man/architecture.md";
    public static final String MANUALPANES_BACKGROUND = "-fx-background-color: rgba(76,28,9,0.3)";

}
