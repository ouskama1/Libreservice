package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;
import com.fort.kickass.view.ui.btns.NewUserBtn;
import com.fort.kickass.view.ui.btns.SearchButton;
import com.google.inject.Injector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * Sets the left-side Users panel.
 */
public class UsersLeftPane extends BorderPane {

    private UsersService usersService;
    private Injector injector;
    private UsersPane usersPane;
    private TextField searchBox;
    private TableView table;
    private String userSearch;

    public UsersLeftPane(UsersPane usersPane, String userSearch, Injector injector) {
        this.injector = injector;
        this.usersPane = usersPane;
        usersService = injector.getInstance(UsersService.class);
        this.userSearch = userSearch;
        init();
        if (userSearch != null) {
            search(userSearch);
        }
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        initTop();
        initLeft();
        initCenter();
        initRight();
        initBottom();
    }

    /**
     * Sets the top panel.
     */
    private void initTop() {
        searchBox = new TextField();
        searchBox.setMinHeight(35);
        searchBox.setMinWidth(120);
        SearchButton search = new SearchButton(this);

        HBox searchBar = new HBox();
        searchBar.setMinHeight(35);
        searchBar.getChildren().addAll(searchBox, search);
        setTop(searchBar);
    }

    /**
     * Sets the left panel.
     */
    private void initLeft() {

    }

    /**
     * Sets the center panel.
     */
    private void initCenter() {
        table = new TableView();
        TableColumn firstName = new TableColumn("First name");
        firstName.setCellValueFactory(new PropertyValueFactory<UsersDto, String>("firstName"));
        TableColumn lastName = new TableColumn("Last name");
        lastName.setCellValueFactory(new PropertyValueFactory<UsersDto, String>("lastName"));
        TableColumn cardNumber = new TableColumn("Card number");
        cardNumber.setCellValueFactory(new PropertyValueFactory<UsersDto, String>("cardNumber"));
        cardNumber.setPrefWidth(130);

        table.setStyle("-fx-background-color: rgba(34, 109, 53, 0.15);");

        table.getColumns().addAll(firstName, lastName, cardNumber);

        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(usersService.getUsers());
        table.setItems(selectedResult);
        setCenter(table);

        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                UsersDto usersDto = (UsersDto) table.getSelectionModel().getSelectedItem();
                usersPane.showUserInfo(usersDto);
            }
        });

    }

    /**
     * Sets the right panel.
     */
    private void initRight() {

    }

    /**
     * Sets the bottom panel.
     */
    private void initBottom() {
        setBottom(new NewUserBtn(this, injector));
    }

    public void search() {
        String detail = searchBox.getText();
        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(usersService.getSearchedUsers(detail));
        table.setItems(selectedResult);
    }

    public void search(String detail) {
        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(usersService.getSearchedUsers(detail));
        table.setItems(selectedResult);
    }

    public void newUser() {
        usersPane.newUser();
    }
}