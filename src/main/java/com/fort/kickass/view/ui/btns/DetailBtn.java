package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.BooksPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Marcel on 25.03.2017.
 */
public class DetailBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/detailBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/detailBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/detailBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private BooksPane booksPane;

    public DetailBtn(BooksPane booksPane) {
        super(100, 50);
        this.booksPane = booksPane;
        setDisable(true);
        init();
    }

    public DetailBtn(boolean reflection, BooksPane booksPane) {
        super(100, 50, reflection);
        this.booksPane = booksPane;
        setDisable(true);
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                booksPane.goToDetail();
            }
        });
    }
}
