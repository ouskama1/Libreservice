package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.AuthorService;
import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.service.impl.GenreService;
import com.fort.kickass.view.dto.AuthorDto;
import com.fort.kickass.view.dto.GenreDto;
import com.fort.kickass.view.dto.ViewDto;
import com.google.inject.Injector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * The center panel for Adding new books, allows adding Authors and Genres.
 */
public class AddBookCenterPane extends BorderPane {

    private Injector injector;
    private BooksPane booksPane;
    private AuthorService authorService;
    private GenreService genreService;
    private BookService bookService;

    private TableView authorTable;
    private TableView genreTable;

    public AddBookCenterPane(Injector injector, BooksPane booksPane) {
        this.injector = injector;
        this.booksPane = booksPane;
        bookService = injector.getInstance(BookService.class);
        authorService = injector.getInstance(AuthorService.class);
        genreService = injector.getInstance(GenreService.class);
        init();
    }

    /**
     * Sets panel's initial settings.
     */
    private void init() {
        Pane topSpacer = new Pane();
        topSpacer.setPrefHeight(40);
        Pane leftSpacer = new Pane();
        leftSpacer.setPrefWidth(60);
        setTop(topSpacer);
        setLeft(leftSpacer);

        authorTable = new TableView();
        genreTable = new TableView();
        authorTable.setPrefWidth(500);
        genreTable.setPrefWidth(500);

        TableColumn authorName = new TableColumn("First name:");
        authorName.setPrefWidth(249);
        authorName.setCellValueFactory(new PropertyValueFactory<AuthorDto, String>("firstName"));
        TableColumn authorLastName = new TableColumn("Last name:");
        authorLastName.setPrefWidth(249);
        authorLastName.setCellValueFactory(new PropertyValueFactory<AuthorDto, String>("lastName"));

        authorTable.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        authorTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                booksPane.setSelectedAuthors((List<AuthorDto>) authorTable.getSelectionModel().getSelectedItems());
                System.out.println();
            }
        });
        genreTable.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        genreTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                booksPane.setSelectedGenres((List<GenreDto>) genreTable.getSelectionModel().getSelectedItems());
                System.out.println();
            }
        });

        authorTable.setStyle("-fx-background-color: rgba(34, 109, 53, 0.15);");
        authorTable.getColumns().addAll(authorName, authorLastName);

        ObservableList<ViewDto> selectedResult = FXCollections.observableArrayList(authorService.getAuthors());
        authorTable.setItems(selectedResult);

        TableColumn genreName = new TableColumn("Genres:");
        genreName.setPrefWidth(498);
        genreName.setCellValueFactory(new PropertyValueFactory<GenreDto, String>("name"));

        genreTable.setStyle("-fx-background-color: rgba(34, 109, 53, 0.15);");
        genreTable.getColumns().addAll(genreName);

        ObservableList<ViewDto> selectedGenreResult = FXCollections.observableArrayList(genreService.getGenres());
        genreTable.setItems(selectedGenreResult);

        HBox panel = new HBox();
        VBox leftPanel = new VBox();
        VBox rightPanel = new VBox();

        panel.setPrefWidth(500);

        leftPanel.getChildren().addAll(new AddBookLeftPane(injector, booksPane));
        rightPanel.getChildren().addAll(authorTable, genreTable);
        panel.getChildren().addAll(leftPanel, rightPanel);

        setCenter(panel);
    }
}
