package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.ui.pane.BooksPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by kommar on 29/05/2017.
 */
public class AddBookBtn extends CustomButton {
    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/newBookBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/newBookBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/newBookBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private Injector injector;
    private BookDetailDto bookDetailDto;

    private BooksPane booksPane;

    public AddBookBtn(BooksPane booksPane, Injector injector) {
        super(100, 50);
        this.booksPane = booksPane;
        this.injector = injector;
        init();
    }

    public AddBookBtn(boolean reflection, BooksPane booksPane, Injector injector) {
        super(100, 50, reflection);
        this.booksPane = booksPane;
        this.injector = injector;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                booksPane.addBook();
            }
        });
    }

    public void setSelectedBook(BookDetailDto bookDetailDto) {
        this.bookDetailDto = bookDetailDto;

    }
}
