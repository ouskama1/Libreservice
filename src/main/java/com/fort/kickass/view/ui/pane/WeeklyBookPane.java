package com.fort.kickass.view.ui.pane;

import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * The panel for the Weekly Book Report.
 */
public class WeeklyBookPane extends GridPane {
    private String title;
    private String authors;
    private int radius = 10;

    public WeeklyBookPane(String title, String authors) {
        this.title = title;
        this.authors = authors;
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        setAlignment(Pos.CENTER);
        Text titleLabel = new Text(title);
        titleLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 10));
        titleLabel.setStyle("    -fx-font-size: 10px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-text-fill: #333333;\n" +
                "    -fx-fill: rgba(53, 129, 11, 0.7);\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");


        Text authorsLabel = new Text(authors);
        authorsLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 10));
        authorsLabel.setStyle("    -fx-font-size: 10px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-text-fill: #333333;\n" +
                "    -fx-fill: rgba(11, 31, 129, 0.7);\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");
        add(titleLabel, 0, 1);

        add(authorsLabel, 0, 3);
        setPrefSize(180, 50);
        setStyle(" -fx-background-color: rgba(129, 14, 14, 0.25);" +
                " -fx-border-radius: " + radius + " " + radius + " " + radius + " " + radius + ";" +
                " -fx-background-radius: " + radius + " " + radius + " " + radius + " " + radius + ";");
    }

}
