package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.HomePane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 24.5.2017..
 */
public class ArchitectureBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/architectureBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/architectureBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/architectureBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private HomePane homePane;

    public ArchitectureBtn(HomePane homePane) {
        super(100, 50, false);
        this.homePane = homePane;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                homePane.goToArchitecture();
            }
        });
    }
}
