package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.BookService;
import com.fort.kickass.view.dto.AuthorDto;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.GenreDto;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.ui.base.Const;
import com.google.inject.Injector;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

/**
 * The main book detail panel.
 */
public class BookDetailPane extends BorderPane {

    private BookService bookService;
    private Injector injector;
    private BookDetailDto book;
    private TableView copiesTable;
    private GridPane detailsPanel;
    private BookReservationPane bookReservationPane;
    private String selectedDateFrom = "";
    private String selectedDateTo = "";
    private MainPane mainPane;
    private int selectedMonth;
    private int selectedYear;

    public BookDetailPane(BookDetailDto book, Injector injector, MainPane mainPane) {
        this.mainPane = mainPane;
        this.injector = injector;
        this.book = book;
        this.bookService = injector.getInstance(BookService.class);
        this.bookReservationPane = new BookReservationPane(mainPane, injector, book, this);
        initTop();
        initCenter(-1, -1);
    }

    /**
     * Initializes top panel.
     */
    public void initTop() {

        VBox topLayer = new VBox();

        Pane topSpacer = new Pane();
        topSpacer.setPrefHeight(Const.BOOK_DETAIL_PANE_TOP_LAYER_HEIGHT);

        String authorPrint = "";

        for (AuthorDto author : book.getAuthors()) {
            if (authorPrint.isEmpty()) {
                authorPrint = author.getFirstName() + " " + author.getLastName();
            } else {
                authorPrint += ", " + author.getFirstName() + " " + author.getLastName();
            }
        }

        Text title = new Text(book.getTitle());
        title.setStyle("-fx-font-size: 32px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-fill: #814446;\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");

        Text authors = new Text(authorPrint);
        authors.setStyle("-fx-font-size: 18px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-fill: #65815f;\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");


        VBox titleBox = new VBox(Const.TOP_MENU_SPACING);
        titleBox.setAlignment(Pos.TOP_CENTER);
        titleBox.getChildren().addAll(title, authors);

        final Pane leftMenuSpacer = new Pane();
        HBox.setHgrow(
                leftMenuSpacer,
                Priority.SOMETIMES
        );

        final Pane rightMenuSpacer = new Pane();
        HBox.setHgrow(
                rightMenuSpacer,
                Priority.SOMETIMES
        );

        final ToolBar toolBar = new ToolBar(
                leftMenuSpacer,
                titleBox,
                rightMenuSpacer
        );
        toolBar.setBackground(Background.EMPTY);
        toolBar.setPrefWidth(400);
        topLayer.getChildren().addAll(topSpacer, toolBar);
        setTop(topLayer);
    }

    /**
     * Initializes center panel.
     *
     * @param month
     * @param year
     */
    public void initCenter(int month, int year) {
        selectedMonth = month;
        selectedYear = year;
        HBox centerLayer = new HBox();

        detailsPanel = new GridPane();
        detailsPanel.setAlignment(Pos.TOP_CENTER);
        detailsPanel.setPrefWidth(Const.BOOK_DETAIL_PANE_DETAILS_WIDTH);

        Label labelYear = new Label("Year   ");
        Label labelPublisher = new Label("Publisher   ");
        Label labelLanguage = new Label("Language   ");
        Label labelIsbn = new Label("ISBN   ");
        Label labelGenres = new Label("Genres   ");
        String genres = "";
        boolean first = true;
        for (GenreDto dto : book.getGenres()) {
            if (first) {
                genres += dto.getName();
                first = false;
            } else {
                genres += ", " + dto.getName();
            }
        }

        detailsPanel.setHalignment(labelYear, HPos.RIGHT);
        detailsPanel.setHalignment(labelPublisher, HPos.RIGHT);
        detailsPanel.setHalignment(labelLanguage, HPos.RIGHT);
        detailsPanel.setHalignment(labelIsbn, HPos.RIGHT);
        detailsPanel.setHalignment(labelGenres, HPos.RIGHT);

        detailsPanel.add(labelYear, 1, 1);
        detailsPanel.add(new Label(book.getPublishedYear() + ""), 2, 1);
        detailsPanel.add(labelPublisher, 1, 2);
        detailsPanel.add(new Label(book.getPublisher()), 2, 2);
        detailsPanel.add(labelLanguage, 1, 3);
        detailsPanel.add(new Label(book.getLanguage()), 2, 3);
        detailsPanel.add(labelIsbn, 1, 4);
        detailsPanel.add(new Label(book.getIsbn()), 2, 4);
        detailsPanel.add(labelGenres, 1, 5);
        detailsPanel.add(new Label(genres), 2, 5);
        VBox detailsWithSchedule = new VBox(100);
        detailsWithSchedule.getChildren().addAll(detailsPanel, new CalendarPane(book, this, injector, month, year));

        Pane rightSpacer1 = new Pane();
        rightSpacer1.setPrefWidth(150);
        Pane rightSpacer2 = new Pane();
        rightSpacer2.setPrefWidth(100);
        Pane bottomSpacer = new Pane();
        bottomSpacer.setPrefHeight(100);
        detailsWithSchedule.setStyle("-fx-background-color: rgba(34, 109, 53, 0.15);" +
                " -fx-border-radius: 80 80 80 80;" +
                " -fx-background-radius: 80 80 80 80;");
        centerLayer.getChildren().addAll(rightSpacer1, detailsWithSchedule, rightSpacer2, bookReservationPane);
        setCenter(centerLayer);
        setBottom(bottomSpacer);
    }

    /**
     * Shows the chosen Reservation on the reservation panel.
     *
     * @param reservation
     */
    public void showReservation(ReservationDto reservation) {
        bookReservationPane.showReservation(reservation);
    }

    public void newReservation(String date) {
        boolean datesSelected = false;
        if (selectedDateFrom.isEmpty()) {
            selectedDateFrom = date;
        } else {
            selectedDateTo = date;
            datesSelected = true;
        }
        bookReservationPane.newReservation(selectedDateFrom, selectedDateTo);
        bookReservationPane.selectPane();
        if (datesSelected) {
            selectedDateTo = "";
            selectedDateFrom = "";
        }
    }

    public void showSelectedPane() {
        bookReservationPane.showSelectedPane();
    }

    public void selectPane() {
        bookReservationPane.selectPane();
    }

    /**
     * Shows New reservation panel.
     *
     * @param date
     */
    public void newReservationShow(String date) {
        if (!selectedDateFrom.isEmpty()) {
            bookReservationPane.newReservation(selectedDateFrom, date);
        } else {
            bookReservationPane.newReservation(date, "");
        }
    }

    public void changeMonth(int month, int year) {
        initCenter(month, year);
    }

    public void refreshMonth() {
        changeMonth(selectedMonth, selectedYear);
    }
}