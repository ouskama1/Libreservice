package com.fort.kickass.view.ui.btns;

import com.fort.kickass.service.impl.ReservationService;
import com.fort.kickass.view.dto.UserReservationDto;
import com.fort.kickass.view.ui.pane.UsersCenterPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class DeleteBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/deleteBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/deleteBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/deleteBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private ReservationService reservationService;
    private Injector injector;
    private UserReservationDto userReservationDto;

    private UsersCenterPane usersCenterPane;

    public DeleteBtn(UsersCenterPane usersCenterPane, Injector injector) {
        super(100, 50);
        this.usersCenterPane = usersCenterPane;
        this.injector = injector;
        reservationService = injector.getInstance(ReservationService.class);
        setDisable(true);
        init();
    }

    public DeleteBtn(boolean reflection, UsersCenterPane usersCenterPane, Injector injector) {
        super(100, 50, reflection);
        this.usersCenterPane = usersCenterPane;
        this.injector = injector;
        reservationService = injector.getInstance(ReservationService.class);
        setDisable(true);
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                reservationService.deleteReservation(userReservationDto.getId());
                usersCenterPane.refresh();
            }
        });
    }

    public void setSelectedReservation(UserReservationDto userReservationDto) {
        this.userReservationDto = userReservationDto;

    }
}
