package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.MainPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Marcel on 26.03.2017.
 */
public class HomeBtn extends MenuButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/homeBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/homeBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/homeBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private MainPane mainPane;

    public HomeBtn(MainPane mainPane) {
        this.mainPane = mainPane;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mainPane.goToHome();
            }
        });
    }
}
