package com.fort.kickass.view.ui.pane;

import com.fort.kickass.service.impl.ReservationService;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.dto.ViewDto;
import com.fort.kickass.view.ui.btns.CalendarBtn;
import com.fort.kickass.view.ui.btns.LeftArrowCalendarButton;
import com.fort.kickass.view.ui.btns.RightArrowCalendarButton;
import com.google.inject.Injector;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

/**
 * The calendar panel for the Reservations.
 */
public class CalendarPane extends GridPane {

    private int selectedMonth;
    private int selectedYear;

    private static final String MONDAY_TAG = "Mon";
    private static final String TUESDAY_TAG = "Tue";
    private static final String WEDNESDAY_TAG = "Wed";
    private static final String THURSDAY_TAG = "Thu";
    private static final String FRIDAY_TAG = "Fri";
    private static final String SATURDAY_TAG = "Sat";
    private static final String SUNDAY_TAG = "Sun";

    private int MON_COLUMN_INDEX = 1;
    private int TUE_COLUMN_INDEX = 2;
    private int WED_COLUMN_INDEX = 3;
    private int THU_COLUMN_INDEX = 4;
    private int FRI_COLUMN_INDEX = 5;
    private int SAT_COLUMN_INDEX = 6;
    private int SUN_COLUMN_INDEX = 7;

    private int monCounter = 2;
    private int tueCounter = 2;
    private int wedCounter = 2;
    private int thuCounter = 2;
    private int friCounter = 2;
    private int satCounter = 2;
    private int sunCounter = 2;

    private BookDetailDto book;
    private ReservationService reservationService;
    private List<ViewDto> currentReservations;
    private Injector injector;
    private BookDetailPane bookDetailPane;


    public CalendarPane(BookDetailDto book, BookDetailPane bookDetailPane, Injector injector, int month, int year) {
        this.book = book;
        this.bookDetailPane = bookDetailPane;
        this.injector = injector;
        this.reservationService = injector.getInstance(ReservationService.class);
        if (month == -1) {
            selectedMonth = 4;
        } else {
            selectedMonth = month;
        }
        if (year == -1) {
            selectedYear = 2017;
        } else {
            selectedYear = year;
        }
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    public void init() {
        setAlignment(Pos.TOP_CENTER);
        add(new LeftArrowCalendarButton(injector, this), 0, 0);
        add(new RightArrowCalendarButton(injector, this), 10, 0);
        add(new Label(MONDAY_TAG), 1, 1);
        add(new Label(TUESDAY_TAG), 2, 1);
        add(new Label(WEDNESDAY_TAG), 3, 1);
        add(new Label(THURSDAY_TAG), 4, 1);
        add(new Label(FRIDAY_TAG), 5, 1);
        add(new Label(SATURDAY_TAG), 6, 1);
        add(new Label(SUNDAY_TAG), 7, 1);
        add(new Label(selectedYear + "  "), 8, 1);
        add(new Label(selectedMonth + 1 + ""), 9, 1);
        Date date = new Date();
        try {
            setMonth(selectedMonth, selectedYear);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void incrementMonth() {
        selectedMonth++;
        if (selectedMonth > 11) {
            selectedMonth = 0;
            selectedYear++;
        }
        bookDetailPane.changeMonth(selectedMonth, selectedYear);
    }

    public void decrementMonth() {
        selectedMonth--;
        if (selectedMonth < 0) {
            selectedMonth = 11;
            selectedYear--;
        }
        bookDetailPane.changeMonth(selectedMonth, selectedYear);
    }

    /**
     * Sets the month.
     *
     * @param month
     * @param year
     * @throws ParseException
     */
    public void setMonth(int month, int year) throws ParseException {
        selectedMonth = month;
        selectedYear = year;
        currentReservations = reservationService.getReservations(month + 1, year, book.getId());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = formatter.parse(year + "-" + (month + 1) + "-1");
        Date endDate = formatter.parse(year + "-" + (month + 1) + "-30");

        LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        boolean first = true;


        for (LocalDate date = start; date.isBefore(end) || date.isEqual(end); date = date.plusDays(1)) {
            if (first) {
                setupCounters(date.getDayOfWeek().toString());
                first = false;
            }
            addDay(date.getDayOfMonth(), date.getDayOfWeek().toString(), getReservation(date.getDayOfMonth()));

        }
    }

    /**
     * Gets the reservation according to the day.
     *
     * @param day
     * @return Reservation
     * @throws ParseException
     */
    private ReservationDto getReservation(int day) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(selectedYear + "-" + (selectedMonth + 1) + "-" + day);
        for (ViewDto dto : currentReservations) {
            ReservationDto reservation = (ReservationDto) dto;
            if (reservation.getDateFrom().getTime().compareTo(date) * date.compareTo(reservation.getExpectedDateTo().getTime()) >= 0) {
                return reservation;
            }
        }
        return null;
    }

    /**
     * Calendar counters.
     *
     * @param dayOfWeek
     */
    private void setupCounters(String dayOfWeek) {
        monCounter = 2;
        tueCounter = 2;
        wedCounter = 2;
        thuCounter = 2;
        friCounter = 2;
        satCounter = 2;
        sunCounter = 2;
        switch (dayOfWeek) {
            case "TUESDAY":
                monCounter++;
                break;
            case "WEDNESDAY":
                monCounter++;
                tueCounter++;
                break;
            case "THURSDAY":
                monCounter++;
                tueCounter++;
                wedCounter++;
                break;
            case "FRIDAY":
                monCounter++;
                tueCounter++;
                wedCounter++;
                thuCounter++;
                break;
            case "SATURDAY":
                monCounter++;
                tueCounter++;
                wedCounter++;
                thuCounter++;
                friCounter++;
                break;
            case "SUNDAY":
                monCounter++;
                tueCounter++;
                wedCounter++;
                thuCounter++;
                friCounter++;
                satCounter++;
                break;
        }
    }

    /**
     * Adds days to the Calendar.
     *
     * @param day
     * @param dayOfWeek
     * @param reservation
     */
    private void addDay(int day, String dayOfWeek, ReservationDto reservation) {
        switch (dayOfWeek) {
            case "MONDAY":
                add(new CalendarBtn(day, reservation, this), MON_COLUMN_INDEX, monCounter);
                monCounter++;
                break;
            case "TUESDAY":
                add(new CalendarBtn(day, reservation, this), TUE_COLUMN_INDEX, tueCounter);
                tueCounter++;
                break;
            case "WEDNESDAY":
                add(new CalendarBtn(day, reservation, this), WED_COLUMN_INDEX, wedCounter);
                wedCounter++;
                break;
            case "THURSDAY":
                add(new CalendarBtn(day, reservation, this), THU_COLUMN_INDEX, thuCounter);
                thuCounter++;
                break;
            case "FRIDAY":
                add(new CalendarBtn(day, reservation, this), FRI_COLUMN_INDEX, friCounter);
                friCounter++;
                break;
            case "SATURDAY":
                add(new CalendarBtn(day, reservation, this), SAT_COLUMN_INDEX, satCounter);
                satCounter++;
                break;
            case "SUNDAY":
                add(new CalendarBtn(day, reservation, this), SUN_COLUMN_INDEX, sunCounter);
                sunCounter++;
                break;
        }
    }

    public void newReservation(int day) {
        bookDetailPane.newReservation(formatInt(day) + "." + formatInt(selectedMonth + 1) + "." + selectedYear);
    }

    public void newReservationShow(int day) {
        bookDetailPane.newReservationShow(formatInt(day) + "." + formatInt(selectedMonth + 1) + "." + selectedYear);
    }

    public String formatInt(int i) {
        return i < 10 ? "0" + i : "" + i;
    }

    public void showSelectedPane() {
        bookDetailPane.showSelectedPane();
    }

    public void selectPane() {
        bookDetailPane.selectPane();
    }

    public void showReservation(ReservationDto reservation) {
        bookDetailPane.showReservation(reservation);
    }
}
