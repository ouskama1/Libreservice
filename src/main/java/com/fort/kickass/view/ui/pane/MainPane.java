package com.fort.kickass.view.ui.pane;

import com.fort.kickass.MainFrame;
import com.fort.kickass.rest.RestClient;
import com.fort.kickass.view.dto.WeeklyBookDto;
import com.fort.kickass.view.dto.WeeklyReportDto;
import com.fort.kickass.view.ui.base.Const;
import com.fort.kickass.view.ui.btns.CatalogBtn;
import com.fort.kickass.view.ui.btns.HomeBtn;
import com.fort.kickass.view.ui.btns.LogoutBtn;
import com.fort.kickass.view.ui.btns.UsersBtn;
import com.google.inject.Injector;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * LibreService Main panel.
 */
public class MainPane extends BorderPane {

    private final static Logger LOGGER = Logger.getLogger(MainPane.class.getName());
    private MainFrame mainFrame;
    private Injector injector;
    public GridPane tabPanel;
    private Pane selectedPane;

    public MainPane(MainFrame mainFrame, Injector injector) {
        this.mainFrame = mainFrame;
        this.injector = injector;
        init();
        initBot();
    }

    /**
     * Sets the panel's initial settings.
     */
    public void init() {
        LogoutBtn logoutBtn = new LogoutBtn(this);
        HomeBtn homeBtn = new HomeBtn(this);
        CatalogBtn catalogBtn = new CatalogBtn(this);
        UsersBtn usersBtn = new UsersBtn(this);

        HBox centerMenu = new HBox(Const.TOP_MENU_SPACING);
        centerMenu.getChildren()
                .addAll(homeBtn, catalogBtn, usersBtn);
        centerMenu.setAlignment(Pos.TOP_CENTER);

        HBox rightMenu = new HBox(Const.TOP_MENU_SPACING);
        rightMenu.getChildren()
                .addAll(logoutBtn);
        rightMenu.setAlignment(Pos.TOP_RIGHT);
        final Pane leftMenuSpacer = new Pane();
        HBox.setHgrow(
                leftMenuSpacer,
                Priority.SOMETIMES
        );

        final Pane rightMenuSpacer = new Pane();
        HBox.setHgrow(
                rightMenuSpacer,
                Priority.SOMETIMES
        );

        final ToolBar toolBar = new ToolBar(
                leftMenuSpacer,
                centerMenu,
                rightMenuSpacer,
                rightMenu
        );
        toolBar.setBackground(Background.EMPTY);
        toolBar.setPrefWidth(400);
        setTop(toolBar);
        goToCatalog();

    }

    /**
     * The bottom panel.
     */
    public void initBot() {
        HBox hBox = new HBox();
        hBox.setPrefHeight(50);
        // load the image
        Image image = new Image(Const.LOADING_GIF);
        // simple displays ImageView the image as is
        ImageView iv1 = new ImageView();
        iv1.setImage(image);
        iv1.setFitHeight(105);
        iv1.setFitWidth(105);
        Pane leftSpacer = new Pane();
        leftSpacer.setPrefSize(500, 0);
        hBox.getChildren().addAll(leftSpacer, iv1);
        setBottom(hBox);

        Task task = new Task<Void>() {
            WeeklyReportDto dto;

            @Override
            public Void call() {
                dto = new RestClient().getWeeklyReport();
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                setWeeklyReport(dto);
            }
        };
        new Thread(task).start();
    }

    /**
     * Sets the Weekly Report.
     *
     * @param dto
     */
    public void setWeeklyReport(WeeklyReportDto dto) {
        VBox vBox = new VBox(5);
        HBox hBox = new HBox();
        hBox.setPrefHeight(60);
        try {
            for (WeeklyBookDto book : dto.getBooks()) {
                Pane leftSpacer = new Pane();
                leftSpacer.setPrefSize(50, 0);
                hBox.getChildren().addAll(leftSpacer, new WeeklyBookPane(book.getTitle(), book.getAuthors()));
            }

            Text booksOfWeekLabel = new Text("Top Books of the week");
            booksOfWeekLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
            booksOfWeekLabel.setStyle("    -fx-font-size: 20px;\n" +
                    "    -fx-font-family: \"Arial Black\";\n" +
                    "    -fx-text-fill: #333333;\n" +
                    "    -fx-fill: rgba(25, 42, 129, 0.7);\n" +
                    "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");
            Text mostFavouriteLibrary = new Text("Favourite library is " + dto.getLibrary());
            mostFavouriteLibrary.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
            mostFavouriteLibrary.setStyle("    -fx-font-size: 15px;\n" +
                    "    -fx-font-family: \"Arial Black\";\n" +
                    "    -fx-text-fill: #333333;\n" +
                    "    -fx-fill: rgba(35, 129, 76, 0.7);\n" +
                    "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");
            vBox.getChildren().addAll(booksOfWeekLabel, hBox, mostFavouriteLibrary);
            hBox.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);
            setBottom(vBox);
        } catch (Exception ex) {

        }
    }

    public void goToCatalog() {
        LOGGER.log(Level.INFO, "Switching to the catalog panel");
        setCenter(new CatalogPane(injector, this));
    }

    public void goToHome() {
        LOGGER.log(Level.INFO, "Switching to the home panel");
        setCenter(new HomePane());
    }

    public void goToUsersPane() {
        LOGGER.log(Level.INFO, "Switching to the users panel");
        setCenter(new UsersPane(injector, this));
    }

    public void goToUsersPane(String user) {
        LOGGER.log(Level.INFO, "Switching to the users panel with a parameter");
        setCenter(new UsersPane(injector, user, this));
    }


    public void logout() {
        mainFrame.setUser(null);
        mainFrame.loginScene();
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

}
