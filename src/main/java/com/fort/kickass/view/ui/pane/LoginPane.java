package com.fort.kickass.view.ui.pane;

import com.fort.kickass.MainFrame;
import com.fort.kickass.base.Utils;
import com.fort.kickass.service.impl.EmployeeService;
import com.fort.kickass.view.dto.EmployeeDto;
import com.fort.kickass.view.ui.btns.LoginBtn;
import com.google.inject.Injector;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * The main login panel.
 */
public class LoginPane extends GridPane {

    private MainFrame mainFrame;
    private Text scenetitle;
    private Label userName;
    private TextField usernameField;
    private Label password;
    private PasswordField passwordField;
    private LoginBtn loginBtn;
    Text actiontarget;
    private Injector injector;
    private EmployeeService employeeService;

    private static final String WRONG_LOGIN_MESSAGE = "Username or password is incorrect.";

    public LoginPane(MainFrame mainFrame, Injector injector) {
        this.mainFrame = mainFrame;
        this.injector = injector;
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    public void init() {
        setAlignment(Pos.CENTER);
        setHgap(10);
        setVgap(10);
        setPadding(new Insets(25, 25, 25, 25));

        scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        add(scenetitle, 0, 0, 2, 1);
        scenetitle.setId("welcome-text");

        userName = new Label("Email:");
        add(userName, 0, 1);

        usernameField = new TextField();
        add(usernameField, 1, 1);
        addEnter(usernameField);

        password = new Label("Password:");
        add(password, 0, 2);

        passwordField = new PasswordField();
        add(passwordField, 1, 2);
        addEnter(passwordField);

        loginBtn = new LoginBtn(false, this);
        add(loginBtn, 1, 3);

        actiontarget = new Text();
        add(actiontarget, 1, 6);
        actiontarget.setVisible(false);
        actiontarget.setText(WRONG_LOGIN_MESSAGE);
        actiontarget.setId("actiontarget");

        employeeService = injector.getInstance(EmployeeService.class);
        GridPane.setHalignment(loginBtn, HPos.RIGHT);
    }

    /**
     * Adds Enter to the element.
     *
     * @param node
     */
    private void addEnter(Node node) {
        node.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    loginBtn.setActiveStyle();
                    login();
                }
            }
        });
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public void login() {
        String mail = usernameField.getText();
        String password = passwordField.getText();
        EmployeeDto employee = (EmployeeDto) employeeService.getEmployee(mail);
        if (employee != null && Utils.passwordHashMatch(password, employee.getPerson().getPassword())) {
            Utils.libraryId = employee.getLibrary().getId();
            mainFrame.setUser(employee);
            mainFrame.homeScene();
        } else {
            actiontarget.setVisible(true);
        }
    }
}
