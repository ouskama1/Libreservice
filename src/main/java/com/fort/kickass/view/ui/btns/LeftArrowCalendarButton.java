package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.CalendarPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class LeftArrowCalendarButton extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/leftArrowBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/leftArrowBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/leftArrowBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private Injector injector;
    private CalendarPane calendarPane;

    public LeftArrowCalendarButton(Injector injector, CalendarPane calendarPane) {
        super(100, 50);
        this.injector = injector;
        this.calendarPane = calendarPane;
        init();
    }

    public LeftArrowCalendarButton(boolean reflection, CalendarPane calendarPane, Injector injector) {
        super(100, 50, reflection);
        this.injector = injector;
        this.calendarPane = calendarPane;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                calendarPane.decrementMonth();
            }
        });
    }

}
