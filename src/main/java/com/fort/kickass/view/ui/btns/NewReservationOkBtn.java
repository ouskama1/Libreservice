package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.BookReservationPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class NewReservationOkBtn extends CustomButton {

    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/okBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/okBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/okBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";


    BookReservationPane bookReservationPane;

    public NewReservationOkBtn(BookReservationPane bookReservationPane) {
        super(100, 50);
        this.bookReservationPane = bookReservationPane;
        setDisable(true);
        init();
    }

    public NewReservationOkBtn(boolean reflection, BookReservationPane bookReservationPane) {
        super(100, 50, reflection);
        this.bookReservationPane = bookReservationPane;
        setDisable(true);
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                bookReservationPane.createReservation();
            }
        });
    }

}
