package com.fort.kickass.view.ui.pane;

import com.fort.kickass.view.ui.btns.ArchitectureBtn;
import com.fort.kickass.view.ui.btns.UserManualBtn;
import com.google.inject.Injector;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * The home panel for LibreService.
 */
public class HomePane extends BorderPane {

    private Injector injector;

    public HomePane() {
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        initTop();
    }

    /**
     * Sets the top panel.
     */
    private void initTop() {
        UserManualBtn userManualBtn = new UserManualBtn(this);
        ArchitectureBtn architectureBtn = new ArchitectureBtn(this);

        HBox centerMenu = new HBox();
        centerMenu.getChildren()
                .addAll(userManualBtn, architectureBtn);

        Pane topSpacer = new Pane();
        topSpacer.setMaxHeight(50);
        topSpacer.getChildren().addAll(centerMenu);
        setTop(topSpacer);
    }

    /**
     * Opens the User manual.
     */
    public void goToUserManual() {
        setCenter(new UserManualPane());
    }

    /**
     * Opens the Architecture manual.
     */
    public void goToArchitecture() {
        setCenter(new ArchitecturePane());
    }

}
