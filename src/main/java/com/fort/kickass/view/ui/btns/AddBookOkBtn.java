package com.fort.kickass.view.ui.btns;

import com.fort.kickass.view.ui.pane.AddBookLeftPane;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by kommar on 29/05/2017.
 */
public class AddBookOkBtn extends CustomButton {
    private final String BTN_DEFAULT_STYLE_PATH = getClass().getResource("/view/icons/okBtnDefault.png").toString();
    private final String BTN_DEFAULT_STYLE = "-fx-background-image: url('" + BTN_DEFAULT_STYLE_PATH + "');";
    private final String BTN_ACTIVE_STYLE_PATH = getClass().getResource("/view/icons/okBtnActive.png").toString();
    private final String BTN_ACTIVE_STYLE = "-fx-background-image: url('" + BTN_ACTIVE_STYLE_PATH + "');";
    private final String BTN_SELECTED_STYLE_PATH = getClass().getResource("/view/icons/okBtnSelected.png").toString();
    private final String BTN_SELECTED_STYLE = "-fx-background-image: url('" + BTN_SELECTED_STYLE_PATH + "');";

    private Injector injector;

    AddBookLeftPane addBookLeftPane;

    public AddBookOkBtn(AddBookLeftPane addBookLeftPane, Injector injector) {
        super(100, 50);
        this.addBookLeftPane = addBookLeftPane;
        this.injector = injector;
        init();
    }

    public AddBookOkBtn(boolean reflection, AddBookLeftPane addBookLeftPane, Injector injector) {
        super(100, 50, reflection);
        this.addBookLeftPane = addBookLeftPane;
        this.injector = injector;
        init();
    }

    public void init() {
        initStyles(BTN_DEFAULT_STYLE, BTN_ACTIVE_STYLE, BTN_SELECTED_STYLE);
        setDefaultStyle();
        initMouseHandlers();
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                addBookLeftPane.createBook();
            }
        });
    }
}
