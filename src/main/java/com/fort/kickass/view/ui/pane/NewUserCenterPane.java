package com.fort.kickass.view.ui.pane;

import com.fort.kickass.base.Utils;
import com.fort.kickass.service.impl.EmployeeService;
import com.fort.kickass.service.impl.LibraryService;
import com.fort.kickass.service.impl.UsersService;
import com.fort.kickass.view.customs.LimitedTextField;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.EmployeeDto;
import com.fort.kickass.view.dto.LibraryDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.ui.btns.NewUserOkBtn;
import com.google.inject.Injector;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * The center panel for adding a new user.
 */
public class NewUserCenterPane extends GridPane {

    private UsersService usersService;
    private EmployeeService employeeService;
    private LibraryService libraryService;
    private Injector injector;
    private UsersPane usersPane;
    private String mainTextsStyle = "    -fx-font-size: 32px;\n" +
            "    -fx-font-family: \"Arial Black\";\n" +
            "    -fx-fill: #818181;\n" +
            "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );";

    private TextField firstNameField;
    private TextField lastNameField;
    private TextField mailField;
    private TextField phoneField;
    private PasswordField passwordOneField;
    private PasswordField passwordTwoField;

    private TextField streetField;
    private TextField houseNumberField;
    private TextField cityField;
    private TextField zipcodeField;
    private TextField countryField;
    private TextArea otherDetailsField;

    private CheckBox isEmployee;

    private Text employeeInfo;

    private Label position;
    private Label since;
    private Label until;

    private TextField positionField;
    private DatePicker sinceField;
    private DatePicker untilField;

    private Text formValidationMessage;


    public NewUserCenterPane(Injector injector, UsersPane usersPane) {
        this.injector = injector;
        this.usersPane = usersPane;
        usersService = injector.getInstance(UsersService.class);
        employeeService = injector.getInstance(EmployeeService.class);
        libraryService = injector.getInstance(LibraryService.class);
        init();
    }

    /**
     * Sets the panel's initial settings.
     */
    private void init() {
        setAlignment(Pos.TOP_CENTER);
        setHgap(10);
        setVgap(10);
        setPadding(new Insets(25, 25, 25, 25));
        String leftSpace = "         ";

        Text personalInfo = new Text("Personal Information");
        personalInfo.setStyle(mainTextsStyle);

        Text address = new Text("Address");
        address.setStyle(mainTextsStyle);

        Label firstName = new Label(leftSpace + "First Name:  ");
        Label lastName = new Label(leftSpace + "Last Name:  ");
        Label mail = new Label(leftSpace + "Mail:  ");
        Label phone = new Label(leftSpace + "Phone:  ");
        Label passwordOne = new Label(leftSpace + "Password:  ");
        Label passwordTwo = new Label(leftSpace + "Password again:  ");

        firstNameField = new TextField();
        lastNameField = new TextField();
        mailField = new TextField();
        phoneField = new TextField();
        passwordOneField = new PasswordField();
        passwordTwoField = new PasswordField();

        isEmployee = new CheckBox();
        isEmployee.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                showEmployeePanel(newValue);
            }
        });

        Text isEmployeeText = new Text("Is an Employee?");
        isEmployeeText.setStyle("-fx-font-size: 15px;\n" +
                "    -fx-font-family: \"Arial Black\";\n" +
                "    -fx-fill: #3f8156;\n" +
                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );");

        add(isEmployeeText, 2, 2);
        add(isEmployee, 3, 2);

        employeeInfo = new Text("Employee info");
        employeeInfo.setStyle(mainTextsStyle);
        position = new Label(leftSpace + "Position");
        since = new Label(leftSpace + "Since");
        until = new Label(leftSpace + "Until");
        positionField = new TextField();
        sinceField = new DatePicker();
        untilField = new DatePicker();

        add(employeeInfo, 2, 3, 2, 1);
        add(position, 2, 4);
        add(since, 2, 5);
        add(until, 2, 6);

        add(positionField, 3, 4);
        add(sinceField, 3, 5);
        add(untilField, 3, 6);

        add(personalInfo, 0, 1, 2, 1);
        add(firstName, 0, 2);
        add(lastName, 0, 3);
        add(mail, 0, 4);
        add(phone, 0, 5);
        add(passwordOne, 0, 6);
        add(passwordTwo, 0, 7);

        add(firstNameField, 1, 2);
        add(lastNameField, 1, 3);
        add(mailField, 1, 4);
        add(phoneField, 1, 5);
        add(passwordOneField, 1, 6);
        add(passwordTwoField, 1, 7);

        Label street = new Label(leftSpace + "Street:  ");
        Label houseNumber = new Label(leftSpace + "House number:  ");
        Label city = new Label(leftSpace + "City:  ");
        Label zipcode = new Label(leftSpace + "Zipcode:  ");
        Label country = new Label(leftSpace + "country:  ");
        Label otherDetails = new Label(leftSpace + "Other details:  ");

        streetField = new TextField();
        houseNumberField = new LimitedTextField(10);
        cityField = new TextField();
        zipcodeField = new LimitedTextField(5);
        countryField = new TextField();
        otherDetailsField = new TextArea();
        otherDetailsField.setPrefHeight(15);

        add(address, 0, 8, 2, 1);
        add(street, 0, 9);
        add(houseNumber, 0, 10);
        add(city, 0, 11);
        add(zipcode, 0, 12);
        add(country, 0, 13);
        add(otherDetails, 0, 14);

        add(streetField, 1, 9);
        add(houseNumberField, 1, 10);
        add(cityField, 1, 11);
        add(zipcodeField, 1, 12);
        add(countryField, 1, 13);
        add(otherDetailsField, 1, 14);
        NewUserOkBtn okBtn = new NewUserOkBtn(this, injector);
        add(okBtn, 1, 15);
        GridPane.setHalignment(okBtn, HPos.RIGHT);

        formValidationMessage = new Text();
        add(formValidationMessage, 2, 15);
        formValidationMessage.setStyle("    -fx-fill: firebrick;\n" +
                "    -fx-font-weight: bold;\n" +
                "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1 );");

        Pane rightTopSpacer = new Pane();
        rightTopSpacer.setPrefHeight(30);
        add(rightTopSpacer, 5, 0);
        showEmployeePanel(false);
    }

    /**
     * Creating a new user.
     */
    public void createUser() {
        if (!isValid()) {
            return;
        }

        UsersDto dto = new UsersDto();
        dto.setFirstName(firstNameField.getText());
        dto.setLastName(lastNameField.getText());
        dto.setEmail(mailField.getText());
        dto.setPhone(phoneField.getText());
        String password = passwordTwoField.getText();
        dto.setPassword(Utils.hashPassword(password));
        dto.setCardNumber(Utils.generateCardNumber());
        AddressDto addrDto = new AddressDto();
        addrDto.setStreet(streetField.getText());
        addrDto.setHouseNumber(Integer.parseInt(houseNumberField.getText()));
        addrDto.setCity(cityField.getText());
        addrDto.setCountry(countryField.getText());
        addrDto.setZipcode(zipcodeField.getText());
        addrDto.setOtherDetails(otherDetailsField.getText());

        dto.setAddress(addrDto);

        if (isEmployee.isSelected()) {
            EmployeeDto eDto = new EmployeeDto();
            eDto.setPerson(dto);
            eDto.setPosition(positionField.getText());
            eDto.setUntil(Utils.localDateToCalendar(untilField.getValue()));
            eDto.setSince(Utils.localDateToCalendar(sinceField.getValue()));
            eDto.setLibrary((LibraryDto) libraryService.getLibrary(Utils.libraryId));
            employeeService.createEmployee(eDto);
        } else {
            usersService.createUser(dto);
        }
        usersPane.refresh(dto.getCardNumber());
    }

    /**
     * Shows the Employee panel.
     *
     * @param show
     */
    private void showEmployeePanel(boolean show) {
        employeeInfo.setDisable(!show);
        position.setDisable(!show);
        since.setDisable(!show);
        until.setDisable(!show);
        positionField.setDisable(!show);
        sinceField.setDisable(!show);
        untilField.setDisable(!show);
    }

    /**
     * Validates user input.
     *
     * @return boolean
     */
    public boolean isValid() {
        if (firstNameField.getText().isEmpty()) {
            formValidationMessage.setText("First name field needs to be filled up");
            return false;
        } else if (lastNameField.getText().isEmpty()) {
            formValidationMessage.setText("Last name field needs to be filled up");
            return false;
        } else if (mailField.getText().isEmpty()) {
            formValidationMessage.setText("Mail field needs to be filled up");
            return false;
        } else if (phoneField.getText().isEmpty()) {
            formValidationMessage.setText("Phone field needs to be filled up");
            return false;
        } else if (passwordOneField.getText().isEmpty()) {
            formValidationMessage.setText("First password field needs to be filled up");
            return false;
        } else if (passwordTwoField.getText().isEmpty()) {
            formValidationMessage.setText("Second password field needs to be filled up");
            return false;
        } else if (!passwordOneField.getText().equals(passwordTwoField.getText())) {
            formValidationMessage.setText("Passwords need to be matching");
            return false;
        } else if (streetField.getText().isEmpty()) {
            formValidationMessage.setText("Street field needs to be filled up");
            return false;
        } else if (houseNumberField.getText().isEmpty()) {
            formValidationMessage.setText("House number field needs to be filled up");
            return false;
        } else if (!isParsableToInt(houseNumberField.getText())) {
            formValidationMessage.setText("House number field needs to be a number");
            return false;
        } else if (zipcodeField.getText().isEmpty()) {
            formValidationMessage.setText("Zipcode field needs to be filled up");
            return false;
        } else if (countryField.getText().isEmpty()) {
            formValidationMessage.setText("Country field needs to be filled up");
            return false;
        } else {
            return true;
        }
    }

    public boolean isParsableToInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}