package com.fort.kickass.view.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class UsersDto extends ViewDto {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String cardNumber;
    private AddressDto address;
    private String password;
    private List<UserReservationDto> reservations = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public List<UserReservationDto> getReservations() {
        return reservations;
    }

    public void setReservations(List<UserReservationDto> reservations) {
        this.reservations = reservations;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}