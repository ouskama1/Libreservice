package com.fort.kickass.view.dto;

import java.util.Calendar;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class EmployeeDto extends ViewDto {

    private int id;
    private Calendar since;
    private Calendar until;
    private String position;
    private LibraryDto library;
    private UsersDto person;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getSince() {
        return since;
    }

    public void setSince(Calendar since) {
        this.since = since;
    }

    public Calendar getUntil() {
        return until;
    }

    public void setUntil(Calendar until) {
        this.until = until;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public LibraryDto getLibrary() {
        return library;
    }

    public void setLibrary(LibraryDto library) {
        this.library = library;
    }

    public UsersDto getPerson() {
        return person;
    }

    public void setPerson(UsersDto person) {
        this.person = person;
    }
}