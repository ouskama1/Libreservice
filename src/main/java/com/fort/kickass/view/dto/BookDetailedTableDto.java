package com.fort.kickass.view.dto;

/**
 * Created by Marcel on 02.04.2017.
 */
public class BookDetailedTableDto extends ViewDto {
    private int id;
    private String publisher;
    private int publishedYear;
    private String language;
    private boolean inThisLibrary;
    private int libraryId;

    public BookDetailedTableDto() {

    }

    public BookDetailedTableDto(int id, String publisher, int publishedYear, String language, boolean inThisLibrary, int libraryId) {
        this.id = id;
        this.publisher = publisher;
        this.publishedYear = publishedYear;
        this.language = language;
        this.inThisLibrary = inThisLibrary;
        this.libraryId = libraryId;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(int publishedYear) {
        this.publishedYear = publishedYear;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isInThisLibrary() {
        return inThisLibrary;
    }

    public void setInThisLibrary(boolean inThisLibrary) {
        this.inThisLibrary = inThisLibrary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }
}
