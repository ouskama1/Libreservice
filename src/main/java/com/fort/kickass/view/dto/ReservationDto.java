package com.fort.kickass.view.dto;

import java.util.Calendar;

/**
 * Created by Lothwen Lórengorm on 17.4.2017..
 */
public class ReservationDto extends ViewDto {

    private int id;
    private UsersDto person;
    private BookDetailDto book;
    private Calendar dateFrom;
    private Calendar expectedDateTo;
    private boolean checkedOut;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UsersDto getPerson() {
        return person;
    }

    public void setPerson(UsersDto person) {
        this.person = person;
    }

    public BookDetailDto getBook() {
        return book;
    }

    public void setBook(BookDetailDto book) {
        this.book = book;
    }

    public Calendar getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Calendar dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Calendar getExpectedDateTo() {
        return expectedDateTo;
    }

    public void setExpectedDateTo(Calendar expectedDateTo) {
        this.expectedDateTo = expectedDateTo;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }
}
