package com.fort.kickass.view.dto;

/**
 * Created by Marcel on 22.05.2017.
 */
public class WeeklyBookDto {
    private String title;
    private String authors;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }
}
