package com.fort.kickass.view.dto;

import com.fort.kickass.persistance.model.Book;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lothwen Lórengorm on 17.4.2017..
 */
public class AuthorDto extends ViewDto {

    private int id;
    private String firstName;
    private String lastName;
    private Set<Book> books = new HashSet();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }
}
