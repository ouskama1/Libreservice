package com.fort.kickass.view.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcel on 22.05.2017.
 */
public class WeeklyReportDto {
    List<WeeklyBookDto> books = new ArrayList<WeeklyBookDto>();
    String library;

    public List<WeeklyBookDto> getBooks() {
        return books;
    }

    public void setBooks(List<WeeklyBookDto> books) {
        this.books = books;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }
}
