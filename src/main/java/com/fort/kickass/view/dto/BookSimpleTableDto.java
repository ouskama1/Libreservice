package com.fort.kickass.view.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcel on 02.04.2017.
 */
public class BookSimpleTableDto extends ViewDto {

    private int id;
    private String title;
    private String author;
    private List<BookDetailedTableDto> detailedResults = new ArrayList();
    private BookDetailedTableDto selectedDetail;

    public BookSimpleTableDto() {

    }

    public BookSimpleTableDto(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<BookDetailedTableDto> getDetailedResults() {
        return detailedResults;
    }

    public void setDetailedResults(List<BookDetailedTableDto> detailedResults) {
        this.detailedResults = detailedResults;
    }

    public BookDetailedTableDto getSelectedDetail() {
        return selectedDetail;
    }

    public void setSelectedDetail(BookDetailedTableDto selectedDetail) {
        this.selectedDetail = selectedDetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
