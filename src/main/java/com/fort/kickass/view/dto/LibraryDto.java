package com.fort.kickass.view.dto;

import java.util.List;

/**
 * Created by Marcel on 28.05.2017.
 */
public class LibraryDto extends ViewDto {
    private int id;
    private String name;
    private String details;
    private AddressDto address;
    private List<BookDetailDto> books;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public List<BookDetailDto> getBooks() {
        return books;
    }

    public void setBooks(List<BookDetailDto> books) {
        this.books = books;
    }
}
