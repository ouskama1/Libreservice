package com.fort.kickass;

import com.fort.kickass.persistance.cdi.interceptor.TransactionalModule;
import com.fort.kickass.view.dto.EmployeeDto;
import com.fort.kickass.view.ui.base.Const;
import com.fort.kickass.view.ui.pane.LoginPane;
import com.fort.kickass.view.ui.pane.MainPane;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Created by Marcel on 25.03.2017.
 */
public class MainFrame extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private TransactionalModule module;
    private Injector injector;
    private Stage primaryStage;
    private EmployeeDto user;

    @Override
    public void start(Stage primaryStage) {
        module = new TransactionalModule();
        injector = Guice.createInjector(module);
        this.primaryStage = primaryStage;
        primaryStage.setTitle(Const.MAIN_FRAME_TITLE);
        loginScene();
        primaryStage.show();
    }

    public void loginScene(){
        switchScene(new LoginPane(this, injector),Const.LOGIN_SCENE_CSS);
    }

    public void homeScene(){
        switchScene(new MainPane(this, injector),Const.HOME_SCENE_CSS);
    }

    public void switchScene(Pane pane, String css){
        Scene scene = new Scene(pane, Const.MAIN_FRAME_SIZE.getWidth(), Const.MAIN_FRAME_SIZE.getHeight());
        primaryStage.setScene(scene);
        scene.getStylesheets().add
                (MainFrame.class.getResource(css).toExternalForm());
        primaryStage.show();
    }

    public void setUser(EmployeeDto user) {
        this.user = user;
    }
}
