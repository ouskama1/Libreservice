package com.fort.kickass.base;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Common static utils used in the whole LibreService application.
 */
public class Utils {

    public static int libraryId = 0;
    public static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    /**
     * Scales the image given in the parameter to the given size.
     *
     * @param icon
     * @param size
     * @return resized image icon
     */
    public static ImageIcon scaleImage(ImageIcon icon, Dimension size) {
        int nw = icon.getIconWidth();
        int nh = icon.getIconHeight();

        if (icon.getIconWidth() > size.getWidth()) {
            nw = (int) size.getWidth();
            nh = (nw * icon.getIconHeight()) / icon.getIconWidth();
        }

        if (nh > size.getHeight()) {
            nh = (int) size.getHeight();
            nw = (icon.getIconWidth() * nh) / icon.getIconHeight();
        }

        return new ImageIcon(icon.getImage().getScaledInstance(nw, nh, Image.SCALE_DEFAULT));
    }

    /**
     * Hashes the given password.
     *
     * @param password
     * @return hashed password
     */
    public static String hashPassword(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     * Checks if the given password matches the given hash.
     *
     * @param password
     * @param hash
     * @return boolean
     */
    public static boolean passwordHashMatch(String password, String hash) {
        return passwordEncoder.matches(password, hash);
    }

    /**
     * Generates a random card number.
     *
     * @return random card number.
     */
    public static String generateCardNumber() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 9).toUpperCase();
    }

    /**
     * Converts local date type to calendar type.
     *
     * @param localDate
     * @return Calendar
     */
    public static Calendar localDateToCalendar(LocalDate localDate) {
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return dateToCalendar(date);
    }

    /**
     * Converts date to calendar.
     *
     * @param date
     * @return Calendar
     */
    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
