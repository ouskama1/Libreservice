package com.fort.kickass.rest;

import com.fort.kickass.view.dto.WeeklyBookDto;
import com.fort.kickass.view.dto.WeeklyReportDto;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

/**
 * Takes care of connecting to remote server.
 */
public class RestClient {
    private final static Logger LOGGER = Logger.getLogger(RestClient.class.getName());
    private static final String WEEKLY_REPORT_URL = "http://localhost:8087/REST";
    private static final String BOOKS_TAG = "books";
    private static final String BOOK_TAG = "book";
    private static final String TITLE_TAG = "title";
    private static final String AUTHOR_TAG = "authors";
    private static final String LIBRARY_TAG = "library";

    /**
     * Gets weekly reports from the remote server.
     *
     * @return WeeklyReport dto
     */
    public WeeklyReportDto getWeeklyReport() {
        while (true) {
            String json = "";
            try {
                URL url = new URL(WEEKLY_REPORT_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                System.out.println("Output from Server .... \n");
                while ((output = br.readLine()) != null) {
                    json += output;
                }

                conn.disconnect();

            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "Problem occured while connecting to the Rest server");
                LOGGER.log(Level.WARNING, e.getMessage());
            }
            try {
                return parseJsonToObject(json);
            } catch (ParseException e) {
                LOGGER.log(Level.WARNING, "Problem occured parsing to jSon");
            }
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Parses String from JSon to Java dto object.
     *
     * @param json
     * @return Java dto object
     * @throws ParseException
     */
    private WeeklyReportDto parseJsonToObject(String json) throws ParseException {
        WeeklyReportDto dto = new WeeklyReportDto();
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(json);
        JSONObject jsonObject = (JSONObject) obj;
        JSONArray books = (JSONArray) jsonObject.get(BOOKS_TAG);
        for (Object objectBook : books) {
            Map jsonBook = (Map) objectBook;
            String title = (String) ((JSONObject) jsonBook.get(BOOK_TAG)).get(TITLE_TAG);
            String authors = (String) ((JSONObject) jsonBook.get(BOOK_TAG)).get(AUTHOR_TAG);
            WeeklyBookDto weeklyBookDto = new WeeklyBookDto();
            weeklyBookDto.setTitle(title);
            weeklyBookDto.setAuthors(authors);
            dto.getBooks().add(weeklyBookDto);
        }
        String library = (String) jsonObject.get(LIBRARY_TAG);
        dto.setLibrary(library);
        return dto;
    }
}
