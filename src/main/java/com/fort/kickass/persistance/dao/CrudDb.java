package com.fort.kickass.persistance.dao;

import com.fort.kickass.persistance.model.DBEntity;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by Marcel on 08.04.2017.
 */
public interface CrudDb {
    /**
     * Creates Entity in the database.
     *
     * @param item
     * @return 0 on unsuccessful create, 1 on successful
     */
    public DBEntity createItem(DBEntity item);

    /**
     * Retrieves all entities from corresponding table
     *
     * @return
     */
    public List<DBEntity> getAllItems();

    /**
     * Retrieves entity based on criteria
     *
     * @param criteria
     * @return Entity
     */

    public List<DBEntity> getItem(Criteria criteria);

    /**
     * Retrieves entity based on it's unique ID.
     *
     * @param id
     * @return Entity
     */
    public DBEntity readItem(int id);

    /**
     * Updates entity in database
     *
     * @param item
     * @return true on successful update, false on unsuccessful
     */
    public boolean updateItem(DBEntity item);

    /**
     * Deletes entity from database
     *
     * @param item
     * @return true on successful delete, false on unsuccessful
     */
    public boolean deleteItem(DBEntity item);
}
