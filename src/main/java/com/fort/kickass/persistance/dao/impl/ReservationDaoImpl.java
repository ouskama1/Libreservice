package com.fort.kickass.persistance.dao.impl;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.dao.CrudDb;
import com.fort.kickass.persistance.model.DBEntity;
import org.hibernate.Criteria;

import java.util.List;

public class ReservationDaoImpl implements CrudDb {

    public ReservationDaoImpl() {

    }

    @Override
    public DBEntity createItem(DBEntity item) {
        HibernateUtil.openedSession.save(item);
        return item;
    }

    @Override
    public List<DBEntity> getAllItems() {
        return null;
    }

    @Override
    public List<DBEntity> getItem(Criteria criteria) {
        return null;
    }

    @Override
    public DBEntity readItem(int id) {
        return null;
    }

    @Override
    public boolean updateItem(DBEntity item) {
        HibernateUtil.openedSession.update(item);
        return true;
    }

    @Override
    public boolean deleteItem(DBEntity item) {
        HibernateUtil.openedSession.delete(item);
        return true;
    }
}
