package com.fort.kickass.persistance.cdi.interceptor;

import com.fort.kickass.persistance.base.HibernateUtil;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.hibernate.Session;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Takes care of opening and closing transaction in between database operations.
 */
public class TransactionalInterceptor implements MethodInterceptor {

    protected Session session;
    private final static Logger LOGGER = Logger.getLogger(TransactionalInterceptor.class.getName());

    /**
     * Invokes itself if transactional annotated method is called.
     *
     * @param methodInvocation
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Object proceed = null;
        try {
            LOGGER.log(Level.INFO, "Calling Service - Opening session");
            HibernateUtil.openedSession = session;
            proceed = methodInvocation.proceed();
            session.getTransaction().commit();
            LOGGER.log(Level.INFO, "Calling Service - Committing session");
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
            HibernateUtil.buildSessionFactory();
        }
        return proceed;
    }
}
