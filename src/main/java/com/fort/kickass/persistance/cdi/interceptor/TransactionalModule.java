package com.fort.kickass.persistance.cdi.interceptor;


import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

/**
 * Created by Marcel on 15.04.2017.
 */
public class TransactionalModule extends AbstractModule {

    @Override
    public void configure() {
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Transactional.class), new TransactionalInterceptor());
    }
}
