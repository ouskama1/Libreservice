package com.fort.kickass.persistance.model;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Lothwen Lórengorm on 27.3.2017..
 */

@Entity
@Table(name = "Employee")
public class Employee extends DBEntity {
    private int id;
    private Calendar since;
    private Calendar until;
    private String position;
    private Library library;
    private Person person;

    public Employee() {
    }

    public Employee(Calendar since, Calendar until, String position) {
        this.since = since;
        this.until = until;
        this.position = position;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_id_seq")
    @SequenceGenerator(name = "employee_id_seq", sequenceName = "employee_id_seq", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "SINCE", nullable = false)
    public Calendar getSince() {
        return since;
    }

    public void setSince(Calendar since) {
        this.since = since;
    }

    @Column(name = "UNTIL", nullable = true)
    public Calendar getUntil() {
        return until;
    }

    public void setUntil(Calendar until) {
        this.until = until;
    }

    @Column(name = "POSITION", length = 128, nullable = false)
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "library_id", nullable = false)
    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}