package com.fort.kickass.persistance.model;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Lothwen Lórengorm on 27.3.2017..
 */

@Entity
@Table(name = "Reservation")
public class Reservation extends DBEntity {
    private int id;
    private Person person;
    private Book book;
    private Calendar dateFrom;
    private Calendar expectedDateTo;
    private boolean checkedOut;

    public Reservation() {
    }

    public Reservation(Calendar dateFrom, Calendar expectedDateTo, boolean checkedOut) {
        this.dateFrom = dateFrom;
        this.expectedDateTo = expectedDateTo;
        this.checkedOut = checkedOut;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_id_seq")
    @SequenceGenerator(name = "reservation_id_seq", sequenceName = "reservation_id_seq", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "person_id", nullable = false)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "book_id", nullable = false)
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Column(name = "DATE_FROM", nullable = false)
    public Calendar getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Calendar dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Column(name = "EXPECTED_DATE_TO", nullable = false)
    public Calendar getExpectedDateTo() {
        return expectedDateTo;
    }

    public void setExpectedDateTo(Calendar expectedDateTo) {
        this.expectedDateTo = expectedDateTo;
    }

    @Column(name = "CHECKED_OUT", nullable = false)
    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }
}