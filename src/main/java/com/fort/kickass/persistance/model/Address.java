package com.fort.kickass.persistance.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lothwen Lórengorm on 27.3.2017..
 */

@Entity
@Table(name = "Address")
public class Address extends DBEntity {
    private int id;
    private String street;
    private int houseNumber;
    private String city;
    private String zipcode;
    private String country;
    private String otherDetails;
    private Set<Library> libraries = new HashSet<>();
    private Set<Person> people = new HashSet<>();

    public Address() {
    }

    public Address(String street, int houseNumber, String city, String zipcode, String country, String otherDetails) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.city = city;
        this.zipcode = zipcode;
        this.country = country;
        this.otherDetails = otherDetails;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_seq")
    @SequenceGenerator(name = "address_id_seq", sequenceName = "address_id_seq", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "STREET", length = 512, nullable = false)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "HOUSE_NUMBER", nullable = false)
    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Column(name = "CITY", length = 512, nullable = false)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "ZIPCODE", length = 10, nullable = false)
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Column(name = "COUNTRY", length = 512, nullable = false)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "OTHER_DETAILS", length = 512, nullable = true)
    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
    public Set<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(Set<Library> libraries) {
        this.libraries = libraries;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
    public Set<Person> getPeople() {
        return people;
    }

    public void setPeople(Set<Person> people) {
        this.people = people;
    }
}
