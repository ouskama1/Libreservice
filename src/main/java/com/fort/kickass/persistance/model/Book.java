package com.fort.kickass.persistance.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lothwen Lórengorm on 27.3.2017..
 */

@Entity
@Table(name = "Book")
public class Book extends DBEntity {
    private int id;
    private String title;
    private int publishedYear;
    private String publisher;
    private int pages;
    private String language;
    private String isbn;
    private Library library;
    private Set<Author> authors = new HashSet<>();
    private Set<Genre> genres = new HashSet<>();

    public Book() {
    }

    public Book(String title, int publishedYear, String publisher, int pages, String language, String isbn) {
        this.title = title;
        this.publishedYear = publishedYear;
        this.publisher = publisher;
        this.pages = pages;
        this.language = language;
        this.isbn = isbn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_id_seq")
    @SequenceGenerator(name = "book_id_seq", sequenceName = "book_id_seq", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "TITLE", length = 512, nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "PUBLISHED_YEAR", nullable = true)
    public int getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(int publishedYear) {
        this.publishedYear = publishedYear;
    }

    @Column(name = "PUBLISHER", length = 512, nullable = true)
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Column(name = "PAGES", nullable = false)
    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Column(name = "LANGUAGE", length = 128, nullable = false)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Column(name = "ISBN", length = 50, nullable = false)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "library_id", nullable = false)
    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "BooksByAuthor", joinColumns = {
            @JoinColumn(name = "BOOK_ID", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID", nullable = false)})
    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "BooksByGenre", joinColumns = {
            @JoinColumn(name = "BOOK_ID", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "GENRE_ID", nullable = false)})
    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

}