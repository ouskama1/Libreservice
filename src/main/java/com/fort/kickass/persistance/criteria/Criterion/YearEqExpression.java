package com.fort.kickass.persistance.criteria.Criterion;

import org.hibernate.Criteria;
import org.hibernate.EntityMode;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;

/**
 * Created by Marcel on 5/6/2017.
 */
public class YearEqExpression implements Criterion {
    private final String propertyName;
    private final int year;

    public YearEqExpression(String propertyName, int year) {
        this.propertyName = propertyName;
        this.year = year;
    }

    @Override
    public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
        String[] columns = criteriaQuery.getColumns(propertyName, criteria);
        if (columns.length != 1) {
            throw new HibernateException("yearEq may only be used with single-column properties");
        }
        return "extract(year from " + columns[0] + ") = ?";
    }

    @Override
    public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
        return new TypedValue[]{new TypedValue(criteriaQuery.getIdentifierType(criteria), year, EntityMode.POJO)};
    }

    @Override
    public String toString() {
        return "extract(year from " + propertyName + ") = " + year;
    }
}