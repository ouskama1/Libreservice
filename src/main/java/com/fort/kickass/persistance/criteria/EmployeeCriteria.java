package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Employee;
import com.fort.kickass.persistance.model.Person;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Criteria to select from the database for Employee entity.
 */
public class EmployeeCriteria extends LibCriteria {

    private final static Logger LOGGER = Logger.getLogger(EmployeeCriteria.class.getName());

    /**
     * Gets an Employee by the given email address.
     *
     * @param email
     * @return Employee
     */
    public DBEntity getEmployee(String email) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Person.class);
        criteria.add(Restrictions.eq("email", email));
        Person person = null;
        try {
            person = (Person) criteria.list().get(0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "No person was found");
            return null;
        }

        criteria = HibernateUtil.openedSession.createCriteria(Employee.class);
        criteria.add(Restrictions.eq("person", person));
        Employee employee = null;
        try {
            employee = (Employee) criteria.list().get(0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Person is not an employee");
        }

        return employee;
    }
}
