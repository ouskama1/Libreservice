package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.criteria.Criterion.MonthEarlierExpression;
import com.fort.kickass.persistance.criteria.Criterion.MonthEqExpression;
import com.fort.kickass.persistance.criteria.Criterion.MonthLaterExpression;
import com.fort.kickass.persistance.criteria.Criterion.YearEqExpression;
import com.fort.kickass.persistance.model.Book;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Person;
import com.fort.kickass.persistance.model.Reservation;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.Calendar;
import java.util.List;

/**
 * Criteria to select from the database for Reservation entity.
 */
public class ReservationCriteria extends LibCriteria {

    /**
     * Gets all the Reservations from the database.
     *
     * @return list of Libraries
     */
    public List<DBEntity> getAllReservations() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Reservation.class);
        return criteria.list();
    }

    /**
     * Gets Reservations from the database according to the given year, month and book id.
     *
     * @param year
     * @param month
     * @param bookId
     * @return list of requested Reservations
     */
    public List<DBEntity> getReservations(int year, int month, int bookId) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Book.class);
        criteria.add(Restrictions.eq("id", bookId));
        Book book = (Book) getResults(criteria).get(0);
        criteria = HibernateUtil.openedSession.createCriteria(Reservation.class);
        criteria.add(Restrictions.eq("book", book));
        criteria.add(new YearEqExpression("dateFrom", year));
        criteria.add(new YearEqExpression("expectedDateTo", year));
        criteria.add(
                Restrictions.or(
                        Restrictions.or(
                                new MonthEqExpression("dateFrom", month)
                                , new MonthEqExpression("expectedDateTo", month))
                        , Restrictions.and(
                                new MonthEarlierExpression("dateFrom", month)
                                , new MonthLaterExpression("expectedDateTo", month))
                ));
        return getResults(criteria);

    }

    /**
     * Gets Reservations from the database according to dates to and from and the book id.
     *
     * @param from
     * @param to
     * @param bookId
     * @return list of requested Reservations
     */
    public List<DBEntity> getReservationsFromTo(Calendar from, Calendar to, int bookId) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Book.class);
        criteria.add(Restrictions.eq("id", bookId));
        Book book = (Book) getResults(criteria).get(0);
        criteria = HibernateUtil.openedSession.createCriteria(Reservation.class);
        criteria.add(Restrictions.eq("book", book));
        criteria.add(Restrictions.or(
                Restrictions.between("dateFrom", from, to)
                , Restrictions.between("expectedDateTo", from, to)
        ));
        return criteria.list();
    }

    /**
     * Gets a Reservation by the given id.
     *
     * @param id
     * @return Reservation
     */
    public Reservation getReservationById(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Reservation.class);
        criteria.add(Restrictions.eq("id", id));
        return (Reservation) criteria.list().get(0);
    }

    /**
     * Gets all the Reservation for a Person by Person's id.
     *
     * @param id
     * @return list of Reservations
     */
    public List<DBEntity> getAllReservationsWithPerson(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Person.class);
        criteria.add(Restrictions.eq("id", id));
        Person person = (Person) criteria.list().get(0);
        criteria = HibernateUtil.openedSession.createCriteria(Reservation.class);
        criteria.add(Restrictions.eq("person", person));
        return criteria.list();
    }
}