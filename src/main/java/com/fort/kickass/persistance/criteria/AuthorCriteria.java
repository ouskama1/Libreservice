package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.Author;
import com.fort.kickass.persistance.model.DBEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Criteria to select from the database for Author entity.
 */
public class AuthorCriteria extends LibCriteria {

    public AuthorCriteria() {
    }

    /**
     * Gets Author by the given id.
     *
     * @param id
     * @return Author
     */
    public Author getAuthor(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Author.class);
        criteria.add(Restrictions.eq("id", id));
        return (Author) getResults(criteria).get(0);
    }

    /**
     * Gets all Authors in the database.
     *
     * @return list of all Authors
     */
    public List<DBEntity> getAllAuthors() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Author.class);
        return criteria.list();
    }
}