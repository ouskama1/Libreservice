package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Person;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Criteria to select from the database for Person/User entity.
 */
public class UsersCriteria extends LibCriteria {

    private final static Logger LOGGER = Logger.getLogger(UsersCriteria.class.getName());

    /**
     * Gets all the Users from the database.
     *
     * @return list of Users
     */
    public List<DBEntity> getAllUsers() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Person.class);
        return criteria.list();
    }

    /**
     * Gets all the Users whose info at least partly matches the given String.
     *
     * @param details
     * @return list of Users
     */
    public List<DBEntity> getSearchedUsers(String details) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Person.class);
        Criterion rest1 = Restrictions.ilike("firstName", "%" + details + "%");
        Criterion rest2 = Restrictions.ilike("lastName", "%" + details + "%");
        Criterion rest3 = Restrictions.ilike("email", "%" + details + "%");
        Criterion rest4 = Restrictions.ilike("cardNumber", "%" + details + "%");
        criteria.add(Restrictions.or(rest1, rest2, rest3, rest4));
        return getResults(criteria);
    }

    /**
     * Gets a User by the given card number.
     *
     * @param cardNumber
     * @return User
     */
    public DBEntity getUserWithCardNumber(String cardNumber) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Person.class);
        criteria.add(Restrictions.eq("cardNumber", cardNumber));
        try {
            return getResults(criteria).get(0);
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "No person with the given card number was found");
            return null;
        }

    }
}
