package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Library;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Criteria to select from the database for Library entity.
 */
public class LibraryCriteria extends LibCriteria {

    /**
     * Gets a Library from the database by the given id.
     *
     * @param id
     * @return Library
     */
    public Library getLibrary(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Library.class);
        criteria.add(Restrictions.eq("id", id));
        return (Library) getResults(criteria).get(0);
    }

    /**
     * Gets all the Libraries from the database.
     *
     * @return list of Libraries
     */
    public List<DBEntity> getAllLibraries() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Library.class);
        return criteria.list();
    }
}