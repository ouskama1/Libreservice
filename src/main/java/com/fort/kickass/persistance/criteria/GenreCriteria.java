package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Genre;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Criteria to select from the database for Genre entity.
 */
public class GenreCriteria extends LibCriteria {

    /**
     * Gets a Genre from the database by the given id.
     *
     * @param id
     * @return Genre
     */
    public Genre getGenre(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Genre.class);
        criteria.add(Restrictions.eq("id", id));
        return (Genre) getResults(criteria).get(0);
    }

    /**
     * Gets all the Genres from the database.
     *
     * @return list of Genres
     */
    public List<DBEntity> getAllGenres() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Genre.class);
        return criteria.list();
    }
}
