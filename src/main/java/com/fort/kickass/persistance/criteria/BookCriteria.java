package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.base.HibernateUtil;
import com.fort.kickass.persistance.model.Book;
import com.fort.kickass.persistance.model.DBEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Criteria to select from the database for Book entity.
 */
public class BookCriteria extends LibCriteria {

    /**
     * Gets a Book by the given id.
     *
     * @param id
     * @return Book
     */
    public Book getBook(int id) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Book.class);
        criteria.add(Restrictions.eq("id", id));
        return (Book) getResults(criteria).get(0);
    }

    /**
     * Gets all the books in the database.
     *
     * @return list of all Books
     */
    public List<DBEntity> getAllBooks() {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Book.class);
        return criteria.list();
    }

    public Book getBookWithIsbn(String isbn) {
        Criteria criteria = HibernateUtil.openedSession.createCriteria(Book.class);
        criteria.add(Restrictions.eq("isbn", isbn));
        return (Book)getResults(criteria).get(0);
    }
}