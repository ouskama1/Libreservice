package com.fort.kickass.persistance.criteria;

import com.fort.kickass.persistance.model.DBEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import java.util.List;

/**
 * Global criteria parent.
 */
public class LibCriteria {
    protected static final String ASCENDANT = "ascend";
    protected static final String DESCENDANT = "descent";
    protected String orderedBy;
    protected String ordering;

    /**
     * Gets sorted results.
     *
     * @param criteria
     * @return list of Results
     */
    protected List<DBEntity> getResults(Criteria criteria) {
        if (ordering != null && orderedBy != null) {
            switch (ordering) {
                case ASCENDANT:
                    criteria.addOrder(Order.asc(orderedBy));
                    break;
                case DESCENDANT:
                    criteria.addOrder(Order.desc(orderedBy));
                    break;
            }
        }
        return criteria.list();
    }
}
