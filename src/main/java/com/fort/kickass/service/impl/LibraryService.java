package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.LibraryCriteria;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.LibraryConverter;
import com.fort.kickass.view.dto.ViewDto;

/**
 * The layer that connects the database and the view.
 */
public class LibraryService extends LibService {

    public LibraryService() {
        dataDtoConverter = new LibraryConverter();
    }

    /**
     * Gets a Library by the given id and converts it to dto.
     *
     * @param id
     * @return Library dto
     */
    @Transactional
    public ViewDto getLibrary(int id) {
        LibraryCriteria criteria = new LibraryCriteria();
        return dataDtoConverter.toDto(criteria.getLibrary(id));
    }
}
