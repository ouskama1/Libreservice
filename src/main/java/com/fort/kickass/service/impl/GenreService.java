package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.GenreCriteria;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.GenreConverter;
import com.fort.kickass.view.dto.ViewDto;

import java.util.List;

/**
 * The layer that connects the database and the view.
 */
public class GenreService extends LibService {

    GenreCriteria genreCriteria;
    GenreConverter genreConverter;

    public GenreService() {
        genreConverter = new GenreConverter();
        genreCriteria = new GenreCriteria();
    }

    /**
     * Gets all the genres and converts them to dtos.
     *
     * @return a list of Genre dtos
     */
    @Transactional
    public List<ViewDto> getGenres() {
        return genreConverter.toDto(genreCriteria.getAllGenres());
    }
}
