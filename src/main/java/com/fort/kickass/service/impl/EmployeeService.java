package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.EmployeeCriteria;
import com.fort.kickass.persistance.criteria.LibraryCriteria;
import com.fort.kickass.persistance.dao.impl.EmployeeDaoImpl;
import com.fort.kickass.persistance.model.Employee;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.EmployeeConverter;
import com.fort.kickass.view.dto.EmployeeDto;
import com.fort.kickass.view.dto.ViewDto;

/**
 * The layer that connects the database and the view.
 */
public class EmployeeService extends LibService {

    public EmployeeService() {
        dataDtoConverter = new EmployeeConverter();
        crud = new EmployeeDaoImpl();
    }

    /**
     * Gets an Employee by the given email and converts it to dto.
     *
     * @param email
     * @return Employee dto
     */
    @Transactional
    public ViewDto getEmployee(String email) {
        EmployeeCriteria criteria = new EmployeeCriteria();
        return dataDtoConverter.toDto(criteria.getEmployee(email));
    }

    /**
     * Creates a new Employee.
     *
     * @param eDto
     */
    @Transactional
    public void createEmployee(EmployeeDto eDto) {
        Employee employee = (Employee) dataDtoConverter.toData(eDto);
        employee.setLibrary(new LibraryCriteria().getLibrary(employee.getLibrary().getId()));
        crud.createItem(employee);
    }
}
