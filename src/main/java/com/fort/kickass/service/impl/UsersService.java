package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.UsersCriteria;
import com.fort.kickass.persistance.dao.impl.UserDaoImpl;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.UsersConverter;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.List;

/**
 * The layer that connects the database and the view.
 */
public class UsersService extends LibService {

    public UsersService() {
        dataDtoConverter = new UsersConverter();
        crud = new UserDaoImpl();
    }

    /**
     * Gets all the Users as dtos.
     *
     * @return a list of User dtos
     */
    @Transactional
    public List<ViewDto> getUsers() {
        UsersCriteria criteria = new UsersCriteria();
        return dataDtoConverter.toDto(criteria.getAllUsers());
    }

    /**
     * Gets a list of Users matching at least partly the given string.
     *
     * @param details
     * @return a list of User dtos
     */
    @Transactional
    public List<ViewDto> getSearchedUsers(String details) {
        UsersCriteria criteria = new UsersCriteria();
        return dataDtoConverter.toDto(criteria.getSearchedUsers(details));
    }

    /**
     * Creates a new User.
     *
     * @param user
     */
    @Transactional
    public void createUser(UsersDto user) {
        crud.createItem(dataDtoConverter.toData(user));
    }

    /**
     * Gets a User by the given card number.
     *
     * @param cardNumber
     * @return User dto
     */
    @Transactional
    public ViewDto getUserWithCardNumber(String cardNumber) {
        UsersCriteria criteria = new UsersCriteria();
        return dataDtoConverter.toDto(criteria.getUserWithCardNumber(cardNumber));
    }

}
