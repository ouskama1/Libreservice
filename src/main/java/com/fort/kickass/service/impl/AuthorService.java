package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.AuthorCriteria;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.AuthorConverter;
import com.fort.kickass.view.dto.ViewDto;

import java.util.List;

/**
 * The layer that connects the database and the view.
 */
public class AuthorService extends LibService {

    AuthorCriteria authorCriteria;
    AuthorConverter authorConverter;

    public AuthorService() {
        authorConverter = new AuthorConverter();
        authorCriteria = new AuthorCriteria();
    }

    /**
     * Gets all the Authors from the database, converted to dtos.
     *
     * @return list of Author dtos
     */
    @Transactional
    public List<ViewDto> getAuthors() {
        return authorConverter.toDto(authorCriteria.getAllAuthors());
    }
}