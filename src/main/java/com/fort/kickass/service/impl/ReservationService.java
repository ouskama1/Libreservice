package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.ReservationCriteria;
import com.fort.kickass.persistance.dao.CrudDb;
import com.fort.kickass.persistance.dao.impl.ReservationDaoImpl;
import com.fort.kickass.persistance.model.Reservation;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.ReservationConverter;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.Calendar;
import java.util.List;

/**
 * The layer that connects the database and the view.
 */
public class ReservationService extends LibService {

    ReservationCriteria reservationCriteria;
    CrudDb reservationDao;

    public ReservationService() {
        reservationCriteria = new ReservationCriteria();
        dataDtoConverter = new ReservationConverter();
        reservationDao = new ReservationDaoImpl();
    }

    /**
     * Gets a Reservation by the given id and sets it to checked/not checked out.
     *
     * @param id
     */
    @Transactional
    public void checkOutReservation(int id) {
        Reservation reservation = reservationCriteria.getReservationById(id);
        boolean status = reservation.isCheckedOut();
        if (status) {
            reservation.setCheckedOut(false);
        } else {
            reservation.setCheckedOut(true);
        }
        reservationDao.updateItem(reservation);
    }

    /**
     * Gets a Reservation by the given id and deletes it.
     *
     * @param id
     */
    @Transactional
    public void deleteReservation(int id) {
        reservationDao.deleteItem(reservationCriteria.getReservationById(id));
    }

    /**
     * Gets Reservations by the given month, year and book id and converts them to dtos.
     *
     * @param month
     * @param year
     * @param bookId
     * @return a list of Reservation dtos
     */
    @Transactional
    public List<ViewDto> getReservations(int month, int year, int bookId) {
        return dataDtoConverter.toDto(reservationCriteria.getReservations(year, month, bookId));
    }

    /**
     * Gets Reservations by the given dates to and from and the book id.
     *
     * @param from
     * @param to
     * @param bookId
     * @return list of Reservation dtos
     */
    @Transactional
    public List<ViewDto> getReservationsFromTo(Calendar from, Calendar to, int bookId) {
        return dataDtoConverter.toDto(reservationCriteria.getReservationsFromTo(from, to, bookId));
    }

    /**
     * Creates a new Reservation
     *
     * @param dto
     */
    @Transactional
    public void createReservation(ReservationDto dto) {
        reservationDao.createItem(dataDtoConverter.toData(dto));
    }
}