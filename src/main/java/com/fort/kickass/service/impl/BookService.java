package com.fort.kickass.service.impl;

import com.fort.kickass.persistance.cdi.interceptor.Transactional;
import com.fort.kickass.persistance.criteria.BookCriteria;
import com.fort.kickass.persistance.dao.impl.BookDaoImpl;
import com.fort.kickass.service.LibService;
import com.fort.kickass.service.converter.impl.BookConverter;
import com.fort.kickass.service.converter.impl.CatalogConverter;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.List;

/**
 * The layer that connects the database and the view.
 */
public class BookService extends LibService {

    BookCriteria bookCriteria;
    BookConverter bookConverter;

    public BookService() {
        dataDtoConverter = new CatalogConverter();
        bookConverter = new BookConverter();
        bookCriteria = new BookCriteria();
        crud = new BookDaoImpl();
    }

    /**
     * Gets a book by the given id and converts it to dto.
     * @param id
     * @return Book dto
     */
    @Transactional
    public BookDetailDto getBookWithIsbn(String isbn) {
        return (BookDetailDto) bookConverter.toDto(bookCriteria.getBookWithIsbn(isbn));
    }

    @Transactional
    public BookDetailDto getBook(int id) {
        return (BookDetailDto) bookConverter.toDto(bookCriteria.getBook(id));
    }

    /**
     * Gets a list of books and converts it to dtos.
     * @return a list of Book dtos
     */
    @Transactional
    public List<ViewDto> getAllBooks() {
        return dataDtoConverter.toDto(bookCriteria.getAllBooks());
    }

    /**
     * Receives a new Book dto, converts it and saves to the database.
     * @param book
     */
    @Transactional
    public void createBook(BookDetailDto book) {
        crud.createItem(new BookConverter().toData(book));
    }

    /**
     * Gets an edited Book dto, converts it and updates in the database.
     * @param dto
     */
    @Transactional
    public void editBook(BookDetailDto dto) {
        crud.updateItem(new BookConverter().toData(dto) );
    }
}
