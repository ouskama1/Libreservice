package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.model.Address;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class AddressConverter implements DataDtoConverter {
    @Override
    public DBEntity toData(ViewDto dto) {
        if (dto == null) {
            return null;
        }
        AddressDto addressDto = (AddressDto) dto;
        Address data = new Address();
        data.setId(addressDto.getId());
        data.setStreet(addressDto.getStreet());
        data.setHouseNumber(addressDto.getHouseNumber());
        data.setCity(addressDto.getCity());
        data.setZipcode(addressDto.getZipcode());
        data.setCountry(addressDto.getCountry());
        data.setOtherDetails(addressDto.getOtherDetails());
        return data;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Address data = (Address) dbEntity;
        AddressDto dto = new AddressDto();
        dto.setId(data.getId());
        dto.setStreet(data.getStreet());
        dto.setHouseNumber(data.getHouseNumber());
        dto.setCity(data.getCity());
        dto.setZipcode(data.getZipcode());
        dto.setCountry(data.getCountry());
        dto.setOtherDetails(data.getOtherDetails());
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList<>();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
