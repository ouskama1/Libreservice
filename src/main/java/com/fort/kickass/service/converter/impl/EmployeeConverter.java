package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Employee;
import com.fort.kickass.persistance.model.Library;
import com.fort.kickass.persistance.model.Person;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.EmployeeDto;
import com.fort.kickass.view.dto.LibraryDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class EmployeeConverter implements DataDtoConverter {

    private UsersConverter usersConverter;

    @Override
    public DBEntity toData(ViewDto dto) {
        if (dto == null) {
            return null;
        }
        usersConverter = new UsersConverter();
        EmployeeDto employeeDto = (EmployeeDto) dto;
        Employee data = new Employee();
        data.setId(employeeDto.getId());
        LibraryConverter libraryConverter = new LibraryConverter();
        data.setLibrary((Library) libraryConverter.toData(employeeDto.getLibrary()));
        data.setPosition(employeeDto.getPosition());
        data.setSince(employeeDto.getSince());
        data.setUntil(employeeDto.getUntil());
        data.setPerson((Person) usersConverter.toData(employeeDto.getPerson()));
        return data;

    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        usersConverter = new UsersConverter();
        Employee data = (Employee) dbEntity;
        EmployeeDto dto = new EmployeeDto();
        dto.setId(data.getId());
        LibraryConverter libraryConverter = new LibraryConverter();
        dto.setLibrary((LibraryDto) libraryConverter.toDto(data.getLibrary()));
        dto.setPosition(data.getPosition());
        dto.setSince(data.getSince());
        dto.setUntil(data.getUntil());
        dto.setPerson((UsersDto) usersConverter.toDto(data.getPerson()));
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList<>();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
