package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.model.Address;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Person;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.UserReservationDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class UsersConverter implements DataDtoConverter {
    @Override
    public DBEntity toData(ViewDto dto) {
        UsersDto usrDto = (UsersDto) dto;
        Person person = new Person();
        Address address = new Address();

        person.setFirstName(usrDto.getFirstName());
        person.setLastName(usrDto.getLastName());
        person.setEmail(usrDto.getEmail());
        person.setPhone(usrDto.getPhone());
        person.setPassword(usrDto.getPassword());
        person.setCardNumber(usrDto.getCardNumber());

        address.setStreet(usrDto.getAddress().getStreet());
        address.setCity(usrDto.getAddress().getCity());
        address.setCountry(usrDto.getAddress().getCountry());
        address.setHouseNumber(usrDto.getAddress().getHouseNumber());
        address.setZipcode(usrDto.getAddress().getZipcode());
        address.setOtherDetails(usrDto.getAddress().getOtherDetails());

        person.setAddress(address);

        return person;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Person data = (Person) dbEntity;
        UsersDto dto = new UsersDto();
        dto.setId(data.getId());
        dto.setFirstName(data.getFirstName());
        dto.setLastName(data.getLastName());
        dto.setEmail(data.getEmail());
        dto.setPhone(data.getPhone());
        dto.setCardNumber(data.getCardNumber());
        dto.setPassword(data.getPassword());

        AddressConverter converterA = new AddressConverter();
        ViewDto addressDto = converterA.toDto(data.getAddress());
        dto.setAddress((AddressDto) addressDto);

        UserReservationConverter converterR = new UserReservationConverter();
        List<DBEntity> reservations = new ArrayList<>(data.getReservations());
        List<UserReservationDto> reservationDtos = new ArrayList<>();
        List<ViewDto> dtos = converterR.toDto(reservations);
        for (ViewDto reservation : dtos) {
            reservationDtos.add((UserReservationDto) reservation);
        }
        dto.setReservations(reservationDtos);
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList<>();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
