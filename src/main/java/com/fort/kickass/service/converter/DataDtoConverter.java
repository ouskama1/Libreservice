package com.fort.kickass.service.converter;

import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.view.dto.ViewDto;

import java.util.List;

/**
 * Converts data to dto and vice versa.
 */
public interface DataDtoConverter {

    /**
     * Converts dto to data.
     *
     * @param dto
     * @return Entity
     */
    public DBEntity toData(ViewDto dto);

    /**
     * Converts data to dto.
     *
     * @param dbEntity
     * @return Dto
     */
    public ViewDto toDto(DBEntity dbEntity);

    /**
     * Converts a list of dtos to a list of entities.
     *
     * @param dto
     * @return list of Entities
     */
    public List<DBEntity> toData(List<ViewDto> dto);

    /**
     * Converts a list of entities to a list of dtos.
     *
     * @param dbEntity
     * @return list of Dtos
     */
    public List<ViewDto> toDto(List<DBEntity> dbEntity);
}
