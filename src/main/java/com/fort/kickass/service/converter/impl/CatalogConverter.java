package com.fort.kickass.service.converter.impl;

import com.fort.kickass.base.Utils;
import com.fort.kickass.persistance.model.Author;
import com.fort.kickass.persistance.model.Book;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.BookDetailedTableDto;
import com.fort.kickass.view.dto.BookSimpleTableDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marcel on 08.04.2017.
 */
public class CatalogConverter implements DataDtoConverter {

    public static int THIS_LIBRARY_ID = Utils.libraryId;

    public DBEntity toData(ViewDto dto) {
        return null;
    }

    public ViewDto toDto(DBEntity dbEntity) {
        return null;
    }

    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    public List<ViewDto> toDto(List<DBEntity> data) {
        HashMap<String, List<BookDetailedTableDto>> map = new HashMap();
        for (DBEntity result : data) {
            Book book = (Book) result;
            String authors = "";
            boolean isFirst = true;
            for (Author author : book.getAuthors()) {
                if (isFirst) {
                    authors += author.getFirstName() + " " + author.getLastName();
                    isFirst = false;
                } else {
                    authors += "\n" + author.getFirstName() + " " + author.getLastName();
                }
            }
            String key = book.getTitle() + ";" + authors;
            BookDetailedTableDto detailedTableDto = new BookDetailedTableDto(
                    book.getId(),
                    book.getPublisher(),
                    book.getPublishedYear(),
                    book.getLanguage(),
                    book.getLibrary().getId() == THIS_LIBRARY_ID,
                    book.getLibrary().getId()
            );
            if (map.containsKey(key)) {
                List<BookDetailedTableDto> detailedTableDtosMap = map.get(key);
                detailedTableDtosMap.add(detailedTableDto);
            } else {
                List<BookDetailedTableDto> tMap = new ArrayList<BookDetailedTableDto>();
                tMap.add(detailedTableDto);
                map.put(key, tMap);
            }
        }
        List<ViewDto> dtos = new ArrayList();
        for (Map.Entry<String, List<BookDetailedTableDto>> entry : map.entrySet()) {
            String key = entry.getKey();
            List<BookDetailedTableDto> values = entry.getValue();
            String[] details = key.split(";");
            String title = details[0];
            String author = details.length > 1 ? details[1] : null;
            BookSimpleTableDto dto = new BookSimpleTableDto(title, author);
            dto.setDetailedResults(entry.getValue());
            dtos.add(dto);
        }
        return dtos;
    }
}