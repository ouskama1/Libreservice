package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.cdi.interceptor.TransactionalModule;
import com.fort.kickass.persistance.criteria.BookCriteria;
import com.fort.kickass.persistance.criteria.UsersCriteria;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Person;
import com.fort.kickass.persistance.model.Reservation;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.ReservationDto;
import com.fort.kickass.view.dto.UsersDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 17.4.2017..
 */
public class ReservationConverter implements DataDtoConverter {
    @Override
    public DBEntity toData(ViewDto dto) {
        if (dto == null) {
            return null;
        }
        ReservationDto reservationDto = (ReservationDto) dto;
        Reservation data = new Reservation();
        data.setCheckedOut(reservationDto.isCheckedOut());
        data.setExpectedDateTo(reservationDto.getExpectedDateTo());
        data.setDateFrom(reservationDto.getDateFrom());
        TransactionalModule module = new TransactionalModule();
        UsersCriteria usersCriteria = new UsersCriteria();
        data.setPerson((Person) usersCriteria.getUserWithCardNumber(reservationDto.getPerson().getCardNumber()));
        BookCriteria bookCriteria = new BookCriteria();
        data.setBook(bookCriteria.getBook(reservationDto.getBook().getId()));
        return data;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Reservation data = (Reservation) dbEntity;
        ReservationDto dto = new ReservationDto();
        dto.setId(data.getId());
        dto.setPerson((UsersDto) new UsersConverter().toDto(data.getPerson()));
        dto.setBook((BookDetailDto) new BookConverter().toDto(data.getBook()));
        dto.setDateFrom(data.getDateFrom());
        dto.setExpectedDateTo(data.getExpectedDateTo());
        dto.setCheckedOut(data.isCheckedOut());
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
