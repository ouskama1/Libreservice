package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.model.Author;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Reservation;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.UserReservationDto;
import com.fort.kickass.view.dto.ViewDto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 14.5.2017..
 */
public class UserReservationConverter implements DataDtoConverter {

    private AuthorConverter authorConverter;

    public UserReservationConverter() {
        authorConverter = new AuthorConverter();
    }

    @Override
    public DBEntity toData(ViewDto dto) {
        return null;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Reservation data = (Reservation) dbEntity;
        UserReservationDto dto = new UserReservationDto();
        dto.setId(data.getId());
        dto.setBookTitle(data.getBook().getTitle());

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String dateFromFormatted = format1.format(data.getDateFrom().getTime());
        dto.setDateFrom(dateFromFormatted);

        String dateToFormatted = format1.format(data.getExpectedDateTo().getTime());
        dto.setDateTo(dateToFormatted);

        dto.setCheckedOut(data.isCheckedOut());

        String authors = "";
        boolean first = true;
        for (Author author : data.getBook().getAuthors()) {
            if (first) {
                authors += author.getFirstName() + " " + author.getLastName();
                first = false;
            } else {
                authors += "\n" + author.getFirstName() + " " + author.getLastName();
            }
        }

        dto.setAuthors(authors);

        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList<>();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
