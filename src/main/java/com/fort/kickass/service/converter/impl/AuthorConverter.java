package com.fort.kickass.service.converter.impl;

import com.fort.kickass.persistance.model.Author;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.AuthorDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 17.4.2017..
 */
public class AuthorConverter implements DataDtoConverter {
    @Override
    public DBEntity toData(ViewDto dto) {
        return null;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Author data = (Author) dbEntity;
        AuthorDto dto = new AuthorDto();
        dto.setId(data.getId());
        dto.setFirstName(data.getFirstName());
        dto.setLastName(data.getLastName());
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
