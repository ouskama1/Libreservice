package com.fort.kickass.service.converter.impl;


import com.fort.kickass.persistance.model.Address;
import com.fort.kickass.persistance.model.Book;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Library;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.AddressDto;
import com.fort.kickass.view.dto.LibraryDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Lothwen Lórengorm on 17.4.2017..
 */
public class LibraryConverter implements DataDtoConverter {
    @Override
    public DBEntity toData(ViewDto dto) {
        if (dto == null) {
            return null;
        }
        LibraryDto libraryDto = (LibraryDto) dto;
        Library data = new Library();

        data.setDetails(libraryDto.getDetails());
        AddressConverter addressConverter = new AddressConverter();
        data.setAddress((Address) addressConverter.toData(libraryDto.getAddress()));
        data.setName(libraryDto.getName());
        data.setId(libraryDto.getId());
        BookConverter bookConverter = new BookConverter();
        Set<Book> list = new HashSet<>();
        for (ViewDto viewDto : libraryDto.getBooks()) {
            list.add((Book) bookConverter.toData(viewDto));
        }
        data.setBooks(list);

        return data;
    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Library data = (Library) dbEntity;
        LibraryDto dto = new LibraryDto();
        dto.setDetails(data.getDetails());
        AddressConverter addressConverter = new AddressConverter();
        dto.setAddress((AddressDto) addressConverter.toDto(data.getAddress()));
        dto.setName(data.getName());
        dto.setId(data.getId());
        BookConverter bookConverter = new BookConverter();
        dto.setBooks(bookConverter.toDto(new ArrayList(data.getBooks())));
        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        return null;
    }
}
