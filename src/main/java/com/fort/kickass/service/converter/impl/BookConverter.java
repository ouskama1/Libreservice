package com.fort.kickass.service.converter.impl;

import com.fort.kickass.base.Utils;
import com.fort.kickass.persistance.criteria.AuthorCriteria;
import com.fort.kickass.persistance.criteria.GenreCriteria;
import com.fort.kickass.persistance.criteria.LibraryCriteria;
import com.fort.kickass.persistance.model.Author;
import com.fort.kickass.persistance.model.Book;
import com.fort.kickass.persistance.model.DBEntity;
import com.fort.kickass.persistance.model.Genre;
import com.fort.kickass.service.converter.DataDtoConverter;
import com.fort.kickass.view.dto.AuthorDto;
import com.fort.kickass.view.dto.BookDetailDto;
import com.fort.kickass.view.dto.GenreDto;
import com.fort.kickass.view.dto.ViewDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Lothwen Lórengorm on 16.4.2017..
 */
public class BookConverter implements DataDtoConverter {

    private AuthorConverter authorConverter;
    private GenreConverter genreConverter;
    private ReservationConverter reservationConverter;

    public BookConverter() {
        authorConverter = new AuthorConverter();
        genreConverter = new GenreConverter();
        reservationConverter = new ReservationConverter();
    }

    @Override
    public DBEntity toData(ViewDto dto) {
        if (dto == null) {
            return null;
        }
        Book data = new Book();
        BookDetailDto bookDetailDto = (BookDetailDto) dto;
        data.setId(bookDetailDto.getId());
        data.setTitle(bookDetailDto.getTitle());
        data.setPublishedYear(bookDetailDto.getPublishedYear());
        data.setPublisher(bookDetailDto.getPublisher());
        data.setPages(bookDetailDto.getPages());
        data.setLanguage(bookDetailDto.getLanguage());
        data.setIsbn(bookDetailDto.getIsbn());
        data.setLibrary(bookDetailDto.getLibrary());

        List<Author> authors = new ArrayList<>();
        AuthorCriteria authorCriteria = new AuthorCriteria();
        List<AuthorDto> authorDtos = bookDetailDto.getAuthors();
        if (authorDtos != null) {
            for (AuthorDto authorDto : bookDetailDto.getAuthors()) {
                authors.add(authorCriteria.getAuthor(authorDto.getId()));
            }
            data.setAuthors(new HashSet(authors));
        }
        List<Genre> genres = new ArrayList<>();
        GenreCriteria genreCriteria = new GenreCriteria();
        List<GenreDto> genreDtos = bookDetailDto.getGenres();
        if (genreDtos != null) {
            for (GenreDto genreDto : bookDetailDto.getGenres()) {
                genres.add(genreCriteria.getGenre(genreDto.getId()));

            }
            data.setGenres(new HashSet(genres));
        }

        LibraryCriteria libraryCriteria = new LibraryCriteria();
        if (bookDetailDto.getLibrary() == null) {
            data.setLibrary(libraryCriteria.getLibrary(Utils.libraryId));
        } else {
            data.setLibrary(bookDetailDto.getLibrary());
        }

        return data;

    }

    @Override
    public ViewDto toDto(DBEntity dbEntity) {
        if (dbEntity == null) {
            return null;
        }
        Book data = (Book) dbEntity;
        BookDetailDto dto = new BookDetailDto();
        dto.setId(data.getId());
        dto.setTitle(data.getTitle());
        dto.setPublishedYear(data.getPublishedYear());
        dto.setPublisher(data.getPublisher());
        dto.setPages(data.getPages());
        dto.setLanguage(data.getLanguage());
        dto.setIsbn(data.getIsbn());
        dto.setLibrary(data.getLibrary());

        List authorsData = new ArrayList(data.getAuthors());
        List<ViewDto> authorViewDtos = authorConverter.toDto(authorsData);
        List<AuthorDto> authorDtos = new ArrayList();
        for (ViewDto dtoTemp : authorViewDtos) {
            authorDtos.add((AuthorDto) dtoTemp);
        }
        dto.setAuthors(authorDtos);

        List genresData = new ArrayList(data.getGenres());
        List<ViewDto> genreViewDtos = genreConverter.toDto(genresData);
        List<GenreDto> genreDtos = new ArrayList();
        for (ViewDto dtoTemp : genreViewDtos) {
            genreDtos.add((GenreDto) dtoTemp);
        }
        dto.setGenres(genreDtos);

        return dto;
    }

    @Override
    public List<DBEntity> toData(List<ViewDto> dto) {
        return null;
    }

    @Override
    public List<ViewDto> toDto(List<DBEntity> dbEntity) {
        List<ViewDto> dtos = new ArrayList<>();
        for (DBEntity entity : dbEntity) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}